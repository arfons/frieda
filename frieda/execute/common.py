"""
.. module:: common
   :platform: Unix, Mac
   :synopsis:  Common utilities for frieda


.. moduleauthor:: Devarshi Ghoshal <dghoshal@lbl.gov>


""" 
import os


def openWithMode(path, mode):
    """ Open file path for writing  and change the permissions to the
        specified mode
        
        :param path: the file path the open for writing
        :param mode: The octal permissions for the file
        :type mode: int 
        
        :return: file descriptor
    """
    f = open(path, 'w')
    os.fchmod(f.fileno(), mode)
    return f
           
class Stack(list):

    def __init__(self):
        self.__data__ = []

    def push(self, item):
        self.__data__.append(item)

    def pop(self):
        return self.__data__.pop()

    def isEmpty(self):
        return (len(self.__data__) == 0)
    
    
def which(program):
    """
        Determine if the specified program exists
        
        :param program: the name of the program
    """
    head, tail = os.path.split(program)
    if not head and tail:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if os.path.isfile(exe_file) and os.access(exe_file, os.X_OK):
                return exe_file
    else:
        if os.path.isfile(head) and os.access(head, os.X_OK):
            return program
        

    return None
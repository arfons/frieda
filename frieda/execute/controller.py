"""
.. module:: controller
   :platform: Unix, Mac
   :synopsis:  Controller Module


.. moduleauthor:: Devarshi Ghoshal <dghoshal@lbl.gov>


"""  
from twisted.internet import defer, defer, reactor, reactor
from twisted.internet.protocol import ProcessProtocol, Protocol, ClientFactory
import logging
import multiprocessing
import os
import sys
import time

import socket
import commands
from frieda.state.statesManager import *
from frieda.state.vectorClock import *
import json
import yaml
from time import sleep
from keywords import Messages,Data

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
LOG = logging.getLogger(__name__)


class ControllerProtocol(Protocol):
    def connectionMade(self):
        VECTOR_CLOCK.updateMasterClockByLocal()        
        manager.put('connectionMade','time', VECTOR_CLOCK)
        LOG.info('Connected to server')
        self.semantics_reloaded = False
    

    def dataReceived(self, dataR):
        data=dataR.split(separator, 1)[0]
        LOG.info('controller: dataReceived: Server responded')
        LOG.info('dataR: %s' % str(dataR))
        LOG.info('data: %s' % str(data))
        
        if data == Messages.ACKNOWLEDGEMENT:
            mode=self.factory.get_partition_type()
            LOG.info("The partition mode is "+mode)
            semantics_file = self.factory.get_semantics_file()
            input_dir = self.factory.get_input_dir()
            partition_type = self.factory.get_partition_type()
            num_parts = self.factory.get_num_parts()
            block_size = self.factory.get_block_size()
            semantics = (lambda:semantics_file, lambda:'None')[semantics_file==None]()
            msg = Messages.MESSAGE_SEMANTICS_RELOAD + ' ' + input_dir + ' ' + \
            semantics + ' ' + partition_type + ' ' + str(num_parts) + ' ' + str(block_size)
            mc=VECTOR_CLOCK.nowMasterClock()
            self.transport.write(msg + mc)#+mc
            self.semantics_reloaded = True
            VECTOR_CLOCK.updateMasterClockByLocal()
            manager.put('dataReceived_msg',msg, VECTOR_CLOCK)
            LOG.info('Input directory (%s) and Semantics file (%s) set with partition-type (%s) and %s partitions' % (input_dir, semantics, partition_type, num_parts))
        else:
            LOG.info('Invalid server response')

    def connectionLost(self, reason):
        VECTOR_CLOCK.updateMasterClockByLocal()
        manager.put('connectionLost',reason,VECTOR_CLOCK)
        LOG.info('Connection lost: %s' % reason)
        self.semantics_reloaded = True
        self.factory.semantics_reload_finished(self.semantics_reloaded)

class ControllerFactory(ClientFactory):
    protocol = ControllerProtocol
    
    def __init__(self, deferred, inputdir, semantics, partition_type, num_parts, block_size):
        self.deferred = deferred
        self.inputdir = inputdir
        self.semantics = semantics
        self.partition_type = partition_type
        self.num_parts = num_parts
        self.block_size = block_size
            

    def semantics_reload_finished(self, reload_flag):
        if self.deferred:
            d, self.deferred = self.deferred, None
            d.callback(reload_flag)
            
    def get_semantics_file(self):
        return self.semantics

    def get_input_dir(self):
        return self.inputdir
    
    def get_partition_type(self):
        return self.partition_type

    def get_num_parts(self):
        return self.num_parts
    
    def get_block_size(self):
        return self.block_size
    
    def clientConnectionFailed(self, connector, reason):
        VECTOR_CLOCK.updateMasterClockByLocal()        
        manager.put('clientConnectionFailed',str(reason), VECTOR_CLOCK)
        LOG.info('Connection failed: %s' % reason)
        if self.deferred:
            d, self.deferred = self.deferred, None
            d.errback(reason)
            
            
            
class Controller(object):
    def __init__(self, inputdir, semantics_file, partition_type, num_parts, block_size=1024):
        self.deferred = defer.Deferred()
        self.factory = ControllerFactory(self.deferred, inputdir, semantics_file, partition_type, num_parts, block_size)

    def connect(self, host, port):
        reactor.connectTCP(host, port, self.factory)
        VECTOR_CLOCK.updateMasterClockByLocal()
        manager.put('connect', str(host)+':'+str(port), VECTOR_CLOCK)

    def reload(self, host, port):
        LOG.info('Starting file transfer')
        self.connect(host, port)
        self.deferred.addCallbacks(self.reload_done, self.reload_failed)
        VECTOR_CLOCK.updateMasterClockByLocal()
        manager.put('reload', str(host)+':'+str(port), VECTOR_CLOCK)


    def reload_done(self, reload_flag):
        LOG.info('Reload of access-semantics done')
        reactor.stop()
        VECTOR_CLOCK.updateMasterClockByLocal()
        manager.put('reload_done',str(reload_flag), VECTOR_CLOCK)

    def reload_failed(self, err):
        LOG.info('Reload failed: %s' % err)
        reactor.stop()
        VECTOR_CLOCK.updateMasterClockByLocal()
        manager.put('reload_failed',str(err), VECTOR_CLOCK)



class ProcessControlProtocol(ProcessProtocol):

    def __init__(self, factory):
        self.factory = factory

    def connectionMade(self):
        LOG.info('Program starting on node- %s' % self.factory.processors[self.factory.num_procs_started])
        self.factory.num_procs_started = self.factory.num_procs_started + 1

    def outReceived(self, data):
        self.factory.outfile_handle.write(data)

    def errReceived(self, err):
        self.factory.errfile_handle.write(err)

    def inConnectionLost(self):
        pass

    def outConnectionLost(self):
        #print 'Program data written to pipe'
        pass

    def processExited(self, reason):
        #print 'In exit'
        pass

    def processEnded(self, reason):
        self.factory.num_procs_completed = self.factory.num_procs_completed + 1
        if self.factory.num_procs_completed == self.factory.num_processors and self.deferred:
            d, self.deferred = self.deferred, None
            d.callback(self.factory.num_procs_completed)
            LOG.info('Program execution completed on all nodes')

  
class ProcessControlFactory():
    def __init__(self, user, key, nodelist, command, outfile, errfile, is_multicore):
        self.num_procs_completed = 0
        self.num_procs_started = 0
        self.user = user
        self.user_key=key
        self.nodelist = nodelist
        self.processors = []
        self.num_processors= len(nodelist)
        self.command = command
        self.outfile = outfile
        self.outfile_handle = open(self.outfile, 'w')
        self.errfile = errfile
        self.errfile_handle = open(self.errfile, 'w')
        self.is_multicore = is_multicore
        
    def remote_fork(self):
        start_time = time.time()

        pcp = ProcessControlProtocol(self)   
        pcp.deferred = defer.Deferred()
        pcp.deferred.addCallback(self.programs_completed)
         
        if self.is_multicore:
            num_cpus = multiprocessing.cpu_count()
            self.num_processors = num_cpus * len(self.nodelist)
            for node in self.nodelist:
                for cpu in range(0, num_cpus):
                    self.processors.append(node + '::CPU-' + str(cpu))
                    auth = self.user+'@' + node
                    cmd = ['ssh', '-i',self.user_key,auth, '-o','StrictHostKeyChecking=no',self.command]
                    reactor.spawnProcess(pcp, 'ssh', cmd, {})
        else:
            for node in self.nodelist:
                self.processors.append(node)
                auth = self.user+'@' + node
                cmd = ['ssh', '-i',self.user_key,auth, '-o','StrictHostKeyChecking=no',self.command]
                reactor.spawnProcess(pcp, 'ssh', cmd, {})
            
        reactor.run()
        
        end_time = time.time() - start_time
        print "Time Elapsed: ", end_time, "secs"
        LOG.info("Total Execution Time: %f secs" % end_time)
        VECTOR_CLOCK.updateMasterClockByLocal()        
        manager.put('app_run_time_secs',str(end_time), VECTOR_CLOCK)
        manager.put('Controller_end', 'time', VECTOR_CLOCK)

    def programs_completed(self, num_procs_completed):
        self.outfile_handle.close()
        self.errfile_handle.close()
        reactor.stop()


def worker_controller(options):

    
    user = options.ssh_user
    user_key = options.ssh_key
    stderr_file = options.stderr_file
    stdout_file = options.stdout_file
    
    nodelist = options.hosts
    server = options.server
    port = options.port
    
    if not options.__dict__['partition']:
        partition_type = Data.DATAPARTITION_REALTIME
    else:
        partition_type = options.partition       
    client_prog = 'frieda execute worker'
    server_param = ' -s ' + server
    port_param = ' -p ' + str(port)
    partition_param = ' -P ' + partition_type
    exec_param = ''
    if options.__dict__['executable']:
        run_command = options.executable
        exec_param = ' -x "' + run_command.replace('$', '\$') + '"'
        
    targetdir = ''
    if options.__dict__['targetdir']:
        targetdir = ' -t ' + options.targetdir
        
    command = client_prog + server_param + port_param + partition_param + exec_param + targetdir
    ##  output options
    command = command + ' -f %s -l %s  -k %s -u %s -D %s -o %s' % (options.ftp, options.file_list, options.key, options.uid, options.machine, options.output_dir) 

    if options.multicore:
        num_cpus = multiprocessing.cpu_count()
        num_procs = num_cpus * len(nodelist)
    else:
        num_procs =  len(nodelist)
                
    LOG.info('User: %s' % user)
    LOG.info('List of nodes: %s' % nodelist)
    LOG.info('Number of processes: %s' % num_procs)
    LOG.info('Client execution command: %s' % command)
    LOG.info('STDOUT file: %s' % stdout_file)
    LOG.info('STDERR file: %s' % stderr_file)
    LOG.info('Output desintation machine: %s' % options.machine)
    LOG.info('Output destination directory: %s' % options.output_dir)
    LOG.info('key: %s' % options.key)
    LOG.info('userid: %s' % options.uid)
    LOG.info('ftp: %s' % options.ftp)
    LOG.info('file_list: %s' % options.file_list)
    
    VECTOR_CLOCK.updateMasterClockByLocal()
    manager.put('user', user, VECTOR_CLOCK)
    manager.put('nodelist',nodelist, VECTOR_CLOCK)
    manager.put('num_procs', num_procs, VECTOR_CLOCK)
    manager.put('client_command',command, VECTOR_CLOCK)
    manager.put('stdout_file',stdout_file, VECTOR_CLOCK)
    manager.put('stderr_file',stderr_file, VECTOR_CLOCK)
    pcf = ProcessControlFactory(user, user_key,nodelist, command, stdout_file, stderr_file, options.multicore)
    pcf.remote_fork()  
    
def master_controller(options):
    
    if not options.__dict__['inputdir']:
        inputdir = '.'
    else:
        inputdir = options.inputdir
    
    if not options.__dict__['semantics']:
        semantics = None
    else:
        semantics = options.semantics
        
    if not options.__dict__['partition']:
        partition = Data.DATAPARTITION_REALTIME
    else:
        partition = options.partition
    
    if not options.__dict__['blocksize']:
        blocksize = 40960
    else:
        blocksize = options.blocksize
        
    if not options.__dict__['blocksize']:
        blocksize = 40960
    else:
        blocksize = options.blocksize
        
    
    if options.__dict__['datamanagement']:
        datamangement = options.datamanagement
        
    num_parts = 1


    if options.partition != Data.DATAPARTITION_EQUAL :
        if options.__dict__['numparts']:
            LOG.info('warning: number of partitions set for %s partition-type will not be used' % options.partition)
    else:    
        if not options.__dict__['numparts']:
            LOG.info('warning: number of partitions is not set, using default partition-size=1')
        else:
            if options.multicore:
                num_cpus = multiprocessing.cpu_count()
                num_parts = num_cpus * options.numparts
            else:
                num_parts =  options.numparts

    reloader = Controller(inputdir, semantics, partition, num_parts,blocksize)

    reloader.reload(options.server, options.port)
    reactor.run()
   
            
def main(args):
########################################
    global manager  #global state manager
    global role
    global separator
    separator = '|:|'
    global VECTOR_CLOCK
    VECTOR_CLOCK = VectorClock(separator)
    role = 'controller'
    print 'controller.py started.'

    manager = stateManagerClient(role, 'file', os.environ['HOME'],socket.gethostname(), getLanIP(), 9160, separator)
    print 'controller.py: manager created.'
    VECTOR_CLOCK.updateMasterClockByLocal()
    manager.put('Controller_start','time', VECTOR_CLOCK)
    print 'controller.py: a k-v pair put.'

    #yamlStr = open(os.environ['HOME'] + '/run_config.yaml','r')
    #dataMap = yaml.load(yamlStr)
    #dataStr= json.dumps(dataMap)

    VECTOR_CLOCK.updateMasterClockByLocal()
    #manager.put('meta',dataStr,VECTOR_CLOCK)
    manager.runCommand('cpuinfo','cat /proc/cpuinfo',VECTOR_CLOCK)
    manager.runCommand('meminfo','cat /proc/meminfo',VECTOR_CLOCK)
    manager.runCommand('osinfo','uname -a',VECTOR_CLOCK)
    sleep(0.5)
    #manager.runCommand('env','env',VECTOR_CLOCK)
########################################

    if args.control=='master':
        LOG.info("Starting the Master Controller")
        master_controller(args)
    else:
        LOG.info("Starting the Worker Controller")
        worker_controller(args)
#####################################################################################           

if __name__ == "__main__":
    exit(main())

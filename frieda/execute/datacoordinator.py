"""
.. module:: datacoordinator
   :platform: Unix
   :synopsis:  The FRIEDA datacoordinator

.. moduleauthor:: Sowmya Balasubramanian <sowmya@es.net>


"""  

from twisted.internet.protocol import Protocol, ServerFactory
from twisted.internet import reactor
from daemon import Daemon
import sys
import logging
import Queue
import subprocess
import os
from keywords import Messages
from paramiko.client import SSHClient, AutoAddPolicy
from paramiko import ChannelFile
from frieda.state import globals
from frieda.state.statesManager import *
from frieda.state.vectorClock import *
from time import sleep
from twisted.protocols.basic import LineReceiver

from frieda.execute.server import FriedaServerFactory, FriedaServerProtocol

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
LOG = logging.getLogger(__name__)


class DataCoordinatorProtocol(FriedaServerProtocol, LineReceiver):

    def connectionMade(self):
        self.client_ip = self.transport.getPeer().host
        self.factory.clients.append(self.client_ip)
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('connectionMade','to_'+str(self.client_ip), globals.VECTOR_CLOCK)
        LOG.info('%s connected (total live clients: %d)' % (self.client_ip, len(self.factory.clients)))
        ack = Messages.ACKNOWLEDGEMENT
        mc=globals.VECTOR_CLOCK.nowMasterClock()
        LineReceiver.MAX_LENGTH = 1024*1024*64
        self.sendLine(ack + mc)#+mc
        
    # Defines the message exchange   
    def dataReceived(self, request):
        LOG.info(request) 
        if(request.startswith(Messages.DATACOORDINATOR_MESSAGE_HANDSHAKE)):
            response = '%s;%s:%s;%s:%s;' % (Messages.MASTER_MESSAGE_CREDENTIALS, \
                                              Messages.CREDENTIALS_FIELD_SOURCE, self.factory.src_host, \
                                              Messages.CREDENTIALS_FIELD_SOURCE_ALIASES, self.factory.src_aliases)
            LOG.info(response)
            self.sendLine(response)    
	elif(request.startswith(Messages.MESSAGE_SEMANTICS_RELOAD)):
	    LOG.info("Reloading data-coordinator semantics file")
	    self.factory.file_list = []
            semantics_info = request.split(' ')
            self.factory.input_path = semantics_info[1]
            semantics_file = semantics_info[2]
            LOG.info('input directory set to: %s' % self.factory.input_path)
            if semantics_file != 'None':
                self.factory.set_file_list(semantics_file)
            else:
                self.factory.set_file_list()
            LOG.info('Semantics file (%s) loaded' % semantics_file)
            LOG.info("Number of task files:"+str(len(self.factory.file_list)))
            self.transport.loseConnection()
        elif(request.startswith(Messages.DATACOORDINATOR_MESSAGE_DATAREQUEST)):
            LOG.info("Received Request: "+Messages.DATACOORDINATOR_MESSAGE_DATAREQUEST)
            req = request.split(":")
            n=1
            if(req[1].isdigit()):
                n = int(req[1])
            else:
                n=1

            filestosend = []
            filestosend = self.factory.getData(n)
            LOG.info(filestosend)
            if not filestosend:
                self.sendLine(Messages.MASTER_MESSAGE_TEARDOWN)
            else:
                str1 = Messages.MESSAGE_VALUES_SEPARATOR.join(filestosend)
                LOG.info("Data-size: " + str(len(str1)))
                self.sendLine(str1)
        elif(request.startswith(Messages.DATACOORDINATOR_MESSAGE_TEARDOWN)):
            self.transport.loseConnection()

    def lineLengthExceeded(self, line):
        LOG.info("Exceeded line-length size: " + str(len(line)) + ", Max-length: " + str(LineReceiver.MAX_LENGTH))
            
# defines the datacoordinator actions               
class DataCoordinatorFactory(FriedaServerFactory):
    protocol = DataCoordinatorProtocol
    
    def __init__(self, input_path, access_semantics_file, partition_type, num_partitions, block_size, ssh_user, ssh_key, src_host, aliases):
        self.src_host = src_host
        self.src_aliases = aliases
        FriedaServerFactory.__init__(self,input_path, access_semantics_file, partition_type, num_partitions, block_size, ssh_user, ssh_key)
        self.set_file_list(access_semantics_file)
        LOG.info("Number of task files:"+str(len(self.file_list)))
        
    #retrieves "n" tasks from queue 
    def getData(self, n):
        filestosend = [];
        i=0
        num_files = len(self.file_list)
        while((i<n) and len(self.file_list)>0):
            filename = self.file_list[0]
            files = filename.split(' ')
            fullFileNames = []
            for file in files:
	        fullFileNames.append(self.input_path+'/'+file)
            fullFile = ' '.join(fullFileNames)     	
            self.remove_element(filename)
            filestosend.append(fullFile)
            i+=1  
        if not filestosend:
            return None
        else: 
            return filestosend

class DataCoordinator(object):
    dataqueue = Queue.Queue()
    def __init__(self, port, input_path, access_semantics_file, partition_type, num_partitions, block_size, ssh_user, ssh_key, src_host, aliases):
        self.port = port
        LOG.info('DataCoordinator initialized')
            
        self.factory = DataCoordinatorFactory(input_path, access_semantics_file, partition_type, num_partitions, block_size, ssh_user, ssh_key, src_host, aliases)

    def listen(self):
        port = reactor.listenTCP(self.port, self.factory)

class DataCoordinatorDaemon(Daemon):
    def __init__(self, pid, sin, sout, serr, args):
        Daemon.__init__(self, pid, sin, sout, serr)
        self.args = args

    def run(self):
        server = DataCoordinator(self.args.port, self.args.path, self.args.semantics, self.args.partition, self.args.numparts, self.args.blocksize, self.args.ssh_user, self.args.ssh_key, self.args.src_host, self.args.src_aliases)
        server.listen()
        reactor.run()
           
def main(args):
    
    globals.init()
    
    vcSeparator = globals.separator
    globals.VECTOR_CLOCK = VectorClock(vcSeparator)
    role = 'datacoordinator'
    print 'datacoordinator.py started.'
    globals.manager = stateManagerClient(role, 'file', os.environ['HOME'], socket.gethostname(), getLanIP(), 9160, vcSeparator)
    print 'datacoordinator.py: manager created.'
    
    globals.VECTOR_CLOCK.updateMasterClockByLocal()
    globals.manager.put('datacoordinator_start','time', globals.VECTOR_CLOCK)
    print 'datacoordinator.py: a k-v pair put.'
    
    globals.manager.runCommand('cpuinfo','cat /proc/cpuinfo', globals.VECTOR_CLOCK)
    globals.manager.runCommand('meminfo','cat /proc/meminfo', globals.VECTOR_CLOCK)
    globals.manager.runCommand('osinfo','uname -a', globals.VECTOR_CLOCK)
    globals.manager.runCommand('env','env', globals.VECTOR_CLOCK)
    sleep(0.5) 
    
    pid = '/tmp/datacoordinator.pid'
    stdin = '/dev/null'
    stdout = '/tmp/datacoordinator_daemon.out'
    stderr = '/tmp/datacoordinator_daemon.err'


    dcdaemon = DataCoordinatorDaemon(pid, stdin, stdout, stderr, args)
    print args.action
    if 'start' == args.action:
        print 'Starting DataCoordinator'
        dcdaemon.start()
    elif 'stop' == args.action:
        print 'Stopping DataCoordinator'
        dcdaemon.stop()

if __name__ == '__main__':
    main(args)
        
        

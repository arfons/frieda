"""
.. module:: worker
   :platform: Unix, Mac
   :synopsis:  The FRIEDA worker

.. moduleauthor:: Devarshi Ghoshal <dghoshal@lbl.gov>


"""  

from common import Stack
from twisted.internet import reactor, defer
from twisted.internet.protocol import ClientFactory, Protocol
import errno
import logging
import optparse
import os
import socket
import subprocess
import time
import uuid
import glob

import pycassa
import socket
import commands
from frieda.state.statesManager import *
from frieda.state.vectorClock import *
from frieda.execute.keywords import Messages
from keywords import Data

my_id = socket.gethostname()
extra_log = {'clientid': my_id}



FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
LOG = logging.getLogger(__name__)


class IntelliWorkerProtocol(Protocol):
 
    def connectionMade(self):
        LOG.info('Connected to server', extra = extra_log)
        self.file_handle = None
        self.files = []
        self.batch_list = []
        self.exec_commands = []
        if self.factory.executable == None:
            self.init_argslist = None
            self.argslist = None
        else:
            self.init_argslist = self.factory.get_params(self.factory.executable)
            self.argslist = self.init_argslist[:]
        self.target_dir = self.factory.target_dir
        self.executable = self.factory.executable
        self.partition_type = self.factory.partition_type
        self.start_time = time.time()
        
        manager.put('connectionMade','to_'+str(self.transport.getPeer().host), VECTOR_CLOCK)
        ''' data-received call: most of the protocol logic is defined here '''

    def dataReceived(self, dataR):
        data=dataR.split(separator, 1)[0]
        masterClock=dataR.split(separator, 1)[1]
        VECTOR_CLOCK.updateMasterClockByValue(masterClock)

        if data == Messages.ACKNOWLEDGEMENT:
            LOG.info('Data received header:'+ Messages.ACKNOWLEDGEMENT, extra = extra_log)
            request_transfer_metadata = Messages.MASTER_MESSAGE_TRANSFER_METADATA
            self.transport.write(request_transfer_metadata)
        elif data.startswith(Messages.WORKER_MESSAGE_NUMBER_OF_TRANSFERS):
            LOG.info('Data received header:'+Messages.WORKER_MESSAGE_NUMBER_OF_TRANSFERS, extra = extra_log)
            self.num_transfers = int(self.get_content(data))
            LOG.info('Transfer size group: %s' % self.num_transfers, extra = extra_log)
            self.transport.write(Messages.MASTER_MESSAGE_FILE_METADATA)
        elif data.startswith(Messages.WORKER_MESSAGE_FILE_METADATA):
            LOG.info('Data received header:'+Messages.WORKER_MESSAGE_FILE_METADATA, extra = extra_log)
            self.file_name = self.get_content(data)
            
            self.file_path = os.path.join(self.target_dir, self.file_name)
            self.batch_list.append(self.file_path)
            if self.executable != None:
                self.place_argument(self.file_path)
            LOG.info('File to be received: %s' % self.file_name, extra = extra_log)
            self.files.append(self.file_name)
            file_exists = self.does_file_exist(self.file_path)
            request_data = Messages.MASTER_MESSAGE_FILE_DATA+' ' + str(file_exists) + ' ' + self.target_dir
            if file_exists:
                LOG.info('File %s exists in directory %s' % (self.file_name, self.target_dir), extra = extra_log)
                self.num_transfers = self.num_transfers - 1
                if self.num_transfers == 0:
                    LOG.info('Batch file transfer complete', extra = extra_log)
                    
                    if self.executable != None:
                        manager.put('dataReceived:Processing_start',str(data), VECTOR_CLOCK)
                        if self.partition_type == Data.DATAPARTITION_REALTIME:
                            LOG.info('Executing program in real-time mode: %s' % str(self.argslist), extra = extra_log)
                            subprocess.call(self.argslist)
                        else:
                            self.exec_commands.append(self.argslist)
                        manager.put('dataReceived:Processing_end',str(data), VECTOR_CLOCK)

                    if self.factory.housekeep:
                        self.do_housekeep()
                        
                    if self.executable != None:
                        self.argslist = self.init_argslist[:]

                    self.batch_list = []

                self.transport.write(request_data)
            else:
                LOG.info('Requesting file %s data' % self.file_name, extra = extra_log)
                self.transport.write(request_data)
        else:
            #LOG.info('File data received', extra = extra_log)
            #self.write_file(data)
            self.file_received(data)

    def get_content(self, data):
        tokens = data.split(' ')
        return tokens[1]

    def does_file_exist(self, file):
        if os.path.isfile(file):
            try:
                with open(file) as f:pass
                return True
            except IOError as e:
                return False
        else:
            return False

    def connectionLost(self, reason):
        LOG.info('Connection lost: %s' % reason, extra = extra_log)
        manager.put('connectionLost','to_'+str(self.transport.getPeer().host), VECTOR_CLOCK)
        self.filesReceived(self.files)
    
    def filesReceived(self, files):
        LOG.info('All files received', extra = extra_log)
        manager.put('filesReceived', str(files), VECTOR_CLOCK)
        self.factory.transfer_finished(files)
        if self.executable != None:
            if self.partition_type != Data.DATAPARTITION_REALTIME:
                transfer_time = time.time() - self.start_time
                LOG.info("Data Transfer Time: %f secs" % transfer_time, extra = extra_log)
                LOG.info('Executing program for %s-partitioned data' % self.partition_type, extra = extra_log)
                LOG.info('%s' % self.exec_commands, extra = extra_log)

                manager.put('filesReceived:Processing_start',str(files), VECTOR_CLOCK)
                self.run_commands(self.exec_commands)
                manager.put('filesReceived:Processing_end',str(files), VECTOR_CLOCK)

    def run_commands(self, command_list):
        #global manager
        #global role
        start_time = time.time()
        i=1

        for command in command_list:
            manager.put('run_commands-'+str(i),command, VECTOR_CLOCK)
            i = i+1
            subprocess.call(command)
        end_time = time.time() - start_time
        manager.put('Task_Execution_Time',str(end_time), VECTOR_CLOCK)
        LOG.info("Task Execution Time: %f secs" % end_time, extra = extra_log)

    def write_file(self, data):
        file_path = os.path.join(self.target_dir, self.file_name)
        if not self.file_handle:
            self.file_handle = open(file_path, 'wb')
        if data.endswith(Messages.WORKER_MESSAGE_END_DATA):
            end_index = len(Messages.WORKER_MESSAGE_END_DATA) * (-1)
            data = data[:end_index]
            self.file_handle.write(data)
            self.file_handle.close()
            self.file_handle = None
            self.num_transfers = self.num_transfers - 1
            LOG.info('File (%s) transferred to directory %s' % (self.file_name, self.target_dir), extra = extra_log)
            if self.num_transfers == 0:
                LOG.info('Batch file_path transfer complete', extra = extra_log)

                if self.executable != None:
                    manager.put('write_file:Processing_start',str(data), VECTOR_CLOCK)
                    if self.partition_type == Data.DATAPARTITION_REALTIME:
                        LOG.info('Executing program in real-time mode: %s' % str(self.argslist), extra = extra_log)
                        subprocess.call(self.argslist)
                    else:
                        LOG.info('Queueing the execution after partitioning in %s mode' % self.partition_type, extra = extra_log)
                        self.exec_commands.append(self.argslist)
                    manager.put('write_file:Processing_end',str(data), VECTOR_CLOCK)

                if self.factory.housekeep:
                    self.do_housekeep()

                if self.executable != None:
                    self.argslist = self.init_argslist[:]
                    
                self.batch_list = []

            self.transport.write(Messages.MASTER_MESSAGE_FILE_METADATA)
        else:
            self.file_handle.write(data)
            
    def file_received(self, data):
        manager.put('file_received',str(self.files), VECTOR_CLOCK)
        #file_path = os.path.join(self.target_dir, self.file_name)
        if data.endswith(Messages.WORKER_MESSAGE_END_DATA):
            end_index = len(Messages.WORKER_MESSAGE_END_DATA) * (-1)
            data = data[:end_index]
            #self.file_handle.write(data)
            #self.file_handle.close()
            #self.file_handle = None
            self.num_transfers = self.num_transfers - 1
            LOG.info('File (%s) transferred to directory %s' % (self.file_name, self.target_dir), extra = extra_log)
            if self.num_transfers == 0:
                LOG.info('Batch file_path transfer complete', extra = extra_log)

                if self.executable != None:
                    manager.put('file_received:Processing_start',str(data), VECTOR_CLOCK)
                    start_time=time.time()
                    if self.partition_type == Data.DATAPARTITION_REALTIME:                        
                            taskwd = "%s/%s" % (os.environ['HOME'],str(uuid.uuid1()))
                            subprocess.call(['mkdir', taskwd])
                            LOG.info('Executing program in real-time mode file_received: %s' % str(self.argslist), extra = extra_log)
                            LOG.info('Working directory: %s' % taskwd, extra = extra_log)
                            subprocess.call(self.argslist, cwd=taskwd)
                            LOG.info('FTP: ' + self.factory.ftp)
                            
                            for flist in self.factory.file_list.split(","):     
                                LOG.info('Processing file_list: ' + os.path.join(taskwd, flist) )
                                for i in glob.glob(os.path.join(taskwd, flist)):  
                                    LOG.info("FTPPPPPPPPPP"+i)                                      
                                    if self.factory.ftp == "globusonline":     
                                        LOG.info('Moving file using globusonline %s to %s@cli.globusonline.org %s:%s ' % (i,self.factory.uid,self.factory.machine, self.factory.output_dir) , extra = extra_log)                                                          
                                        ftp_cmd = "ssh -o StrictHostKeyChecking=no -i ~/.ssh/%s %s@cli.globusonline.org scp %s:%s %s:%s" % (self.factory.key, self.factory.uid, socket.gethostname(), i, self.factory.machine, self.factory.output_dir)
                                        LOG.info('FTP command: %s' % ftp_cmd)                                                                                                  
                                    elif self.factory.ftp == "scp":    
                                        ## if output data need to be transferred to Master, get the master Ip address
                                        if self.factory.machine == "master":
                                            self.factory.machine = os.getenv('MASTER_PRIVATE_IP')

                                        LOG.info('SCPing %s to %s@%s:%s ' % (i,self.factory.uid,self.factory.machine, self.factory.output_dir) , extra = extra_log)
                                        ftp_cmd = "scp -o StrictHostKeyChecking=no -i %s %s %s@%s:%s" % (self.factory.key, i, self.factory.uid, self.factory.machine, self.factory.output_dir)
                                        LOG.info('FTP command: %s' % ftp_cmd)   
                                        
                                    subprocess.call([ftp_cmd], cwd=taskwd, shell=True)                                      
                            subprocess.call(['rm -fr *'], cwd=taskwd, shell=True)                                                                     
                    else:
                        LOG.info('Queueing the execution after partitioning in %s mode' % self.partition_type, extra = extra_log)
                        self.exec_commands.append(self.argslist)
                    end_time=time.time()
                    manager.put('file_received:Processing_end',str(data), VECTOR_CLOCK)
                    manager.put('Task_Execution_Time:', str(end_time - start_time), VECTOR_CLOCK)
                if self.factory.housekeep:
                    self.do_housekeep()

                if self.executable != None:
                    self.argslist = self.init_argslist[:]
                    
                self.batch_list = []

            self.transport.write(Messages.MASTER_MESSAGE_FILE_METADATA)
        else:
            #self.file_handle.write(data)
            print 'Unknown option'

    def do_housekeep(self):
        LOG.info('Deleting files (%s)' % str(self.batch_list), extra = extra_log)
        manager.put('do_housekeep','time', VECTOR_CLOCK)
        for file_name in self.batch_list:
            if self.does_file_exist(file_name):
                os.unlink(file_name)

    def place_argument(self, actual_arg):
        for index,arg in enumerate(self.argslist):
            if arg.startswith('$'):
                self.argslist[index] = actual_arg
                break


class IntelliWorkerFactory(ClientFactory):
    protocol = IntelliWorkerProtocol

    def __init__(self, deferred, target_dir, executable, partition_type, housekeep, uid, ftp, key, machine, output_dir, file_list):
        LOG.info('Client factory initialized', extra = extra_log)
        
        self.deferred = deferred
        self.target_dir = target_dir
        self.executable = executable
        self.housekeep = housekeep
        self.partition_type = partition_type
        
        self.uid = uid
        self.ftp = ftp
        self.key = key
        self.machine = machine
        self.output_dir = output_dir
        self.file_list = file_list        
    

    def transfer_finished(self, files):
        if self.deferred:
            d, self.deferred = self.deferred, None
            d.callback(files)

    def clientConnectionFailed(self, connector, reason):
        LOG.error('Connection failed: %s' % reason, extra = extra_log)
        manager.put('clientConnectionFailed',reason, VECTOR_CLOCK)
        if self.deferred:
            d, self.deferred = self.deferred, None
            d.errback(reason)

    def get_params(self, cmd):
        global manager
        global role
        manager.put('command',cmd, VECTOR_CLOCK)
        delim_stack = Stack()
        arg_stack = Stack()
        argv = []
        arg = ''
        for char in cmd:
            if char == '"' or char == "'":
                if delim_stack.isEmpty():
                    delim_stack.push(char)
                else:
                    quote = delim_stack.pop()
                    if quote == char:
                        arg = ''
                        while not arg_stack.isEmpty():
                            arg = arg_stack.pop() + arg
                        if arg != '':
                            argv.append(arg)
                            arg= ''
                    else:
                        delim_stack.push(quote)
                        delim_stack.push(char)
            elif char == ' ' and delim_stack.isEmpty():
                arg = ''
                while not arg_stack.isEmpty():
                    arg = arg_stack.pop() + arg
                if arg != '':
                    argv.append(arg)
                    arg= ''
            else:
                arg_stack.push(char)
        arg = ''
        while not arg_stack.isEmpty():
            arg = arg_stack.pop() + arg
        if arg != '':
            argv.append(arg)
        return argv


class IntelliWorker(object):
    def __init__(self, target_dir, executable, partition_type, housekeep, uid, ftp, key, machine, output_dir, file_list ):
        LOG.info('Client initialized', extra = extra_log)
        
        self.deferred = defer.Deferred()
        self.executable = executable
        self.target_dir = self.check_target_directory(target_dir)
        self.factory = IntelliWorkerFactory(self.deferred, self.target_dir, executable, partition_type, housekeep, uid, ftp, key, machine, output_dir, file_list)
        self.files = []

    def check_target_directory(self, target_dir):
        try:
            os.makedirs(target_dir)
        except OSError as e:
            if e.errno != errno.EEXIST:
                LOG.info('Client initialized', extra = extra_log)
        return target_dir
            
    def connect(self, host, port):
        reactor.connectTCP(host, port, self.factory)
        LOG.info('Connecting to server %s on port %d'% (host, port), extra = extra_log)

    def files_received(self, files):
        #manager.put('files_received',str(files))
        LOG.info('List of files received: %s' % files, extra = extra_log)
        reactor.stop()
        manager.put(role + '_end','time', VECTOR_CLOCK)


    def transfer_failed(self, err):
        manager.put('transfer_failed', err, VECTOR_CLOCK)
        LOG.info('Transfer failed with error: %s' % err, extra = extra_log)
        reactor.stop()
        manager.put(role + '_end','time', VECTOR_CLOCK)

    def start_file_transfer(self, host, port):
        #manager.put('start_file_transfer', str(host)+':'+str(port))
        LOG.info('Starting file transfer', extra = extra_log)
        self.connect(host, port)
        self.deferred.addCallbacks(self.files_received, self.transfer_failed)

def main(args):
    global manager
    global role
    role = 'worker'
    print 'worker.py started.'
    global separator
    separator = '|:|'
    manager = stateManagerClient(role, 'file', os.environ['HOME'], socket.gethostname(), getLanIP(), 9160, separator)
    print 'worker.py: manager created.'
    global VECTOR_CLOCK
    VECTOR_CLOCK = VectorClock(separator)
    VECTOR_CLOCK.updateMasterClockByLocal()
    manager.put(role + '_start','time', VECTOR_CLOCK)
    print 'worker.py: a k-v pair put.'
    manager.runCommand('cpuinfo','cat /proc/cpuinfo', VECTOR_CLOCK)
    manager.runCommand('meminfo','cat /proc/meminfo', VECTOR_CLOCK)
    manager.runCommand('osinfo','uname -a', VECTOR_CLOCK)
    manager.runCommand('env','env', VECTOR_CLOCK)
#####################################

    if args.partition == Data.DATAPARTITION_REALTIME:
        if not args.__dict__['executable']:
            raise Exception('Executable command must be given in order to partition data in real-time')

    client = IntelliWorker(args.targetdir, args.executable, args.partition, args.housekeep, args.uid, args.ftp, args.key, args.machine, args.output_dir, args.file_list)
    client.start_file_transfer(args.server, args.port)
    reactor.run()

if __name__ == '__main__':
    main()

import frieda

def main(args):

    # parse arguments
    # args = parse_args()

    # generate updated cloud monkey configuration with storage plan details

    cm_config = args.__dict__['cm_config']
    actions = " ".join(args.__dict__['actions'].split(","))
    machine_id = " ".join(args.__dict__['machine_id'].split(","))

    frieda.util.integration.friedamonkey(cm_config, 'preparation', actions, machine_id )


if __name__ == "__main__":
    main()

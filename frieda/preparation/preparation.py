'''
Created on Jan 29, 2013

@author: val
'''
import os
import logging
import threading
import Queue
from collections import namedtuple
import frieda.util.cloudutil as cloudutil


WORKERS = 10

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
LOG = logging.getLogger(__name__)


class Action:
    """
        A Action performs preparation of the named action.
        
        :param cloud_config: The Cloud configuration
        :type cloud_config: config.CloudConfig
        :param name: The name of the the action to apply
    """

    def __init__(self, cloud_config, phase, name):
        self.cloud_config = cloud_config
        self.name = name

        self.ctxt = cloud_config.actions(name)

        self.commands = []
        if self.ctxt.has_key('commands'):
            self.commands = self.ctxt['commands']
        self.template = None
        if self.ctxt.has_key('template'):
            self.params = self.ctxt['params']
            self.ctxt['params']
            self.template = self.ctxt['template']
        self.keypair = None
        if self.ctxt.has_key('keypair'):
            self.keypair = self.ctxt['keypair']
        self.files = []
        if self.ctxt.has_key('files'):
            self.files = self.ctxt['files']
        self.cloud_params = self.cloud_config.cloud()
        if self.cloud_params['platform'] == 'orca':
            self.ssh_keys = [os.path.expanduser("~/.ssh/%s" % self.cloud_params['keypair'])]
        else:
            self.ssh_keys = [os.path.expanduser("~/.ssh/%s.pem" % self.cloud_params['keypair'])]


    def apply(self, instance_id):
        """
           Apply the action to the specified instance
           
           :param instance_id: instance id of the node to prepare
        
        """
        try:
            conn = cloudutil.getConnection(self.cloud_params)
        except:
            conn = None

        if conn:
            instance = cloudutil.getNode(instance_id, conn)

            ssh_interface = 'public_ips'
            if 'ssh_interface' in self.cloud_params:
                ssh_interface = self.cloud_params['ssh_interface']
            ip_addresses = getattr(instance, ssh_interface)
            ip = ip_addresses[len(ip_addresses)-1]
        else:
            ip = instance_id

        logfile = "%s/%s-%s.log" % (os.getcwd(), self.name, instance_id)
        outfile = open(logfile, 'w')
        LOG.info("logging to %s" % logfile)
        for c in self.commands:
            LOG.info("applying %s command %s" % (self.name, c))
            outfile.write("applying %s command %s\n" % (self.name, c))
            try:
                cloudutil.run(ip,
                          self.cloud_params['ssh_user'], self.ssh_keys, c, outfile=outfile)
            except:
                pass

        
        for f in self.files:
            LOG.info("uploading %s to %s" % (f[0], f[1]))
            try:
                if os.path.isdir(f[0]):
                    LOG.info("rsync %s to %s" % (f[0], f[1]))
                    cloudutil.rsyncDir(ip, self.cloud_params['ssh_user'], self.ssh_keys[0], f[0], f[1])
                else:
                    LOG.info("scp %s to %s" % (f[0], f[1]))
                    cloudutil.scpFile(ip, self.cloud_params['ssh_user'], self.ssh_keys[0], f[0], f[1])
            except:
                pass
                
        if self.template:
            LOG.info("applying %s template %s" % (self.name, self.template))
            cloudutil.applyPuppet(ip_address=ip,
                                  ssh_user=self.cloud_params['ssh_user'],
                                  filename="%s.pp" % self.name,
                                  template=self.template,
                                  params=dict(os.environ.items() + self.params.items()),
                                  ssh_keys=self.ssh_keys, outfile=outfile)
        if self.keypair:
            cloudutil.putSSHKey(ip_address=ip,
                                ssh_user=self.cloud_params['ssh_user'], ssh_keys=self.ssh_keys
                                , user=self.keypair['user'], user_home=self.keypair['user_dir'], pem_file=self.keypair['keyfile'])


class InstancePreparator(object):
    """ Preparation thread execution for parallelism. Each worker prepares an
        entire instance.
    
    
    :param cloudConfig: cloud configuration parameters
    :type cloudConfig: config.CloudConfig
    :param num_threads: The number of concurrent threads to launch in parallel
    :type num_threads: int
    
    """

    ActionTuple = namedtuple('ActionTuple', 'instanceId phase actions')

    def __init__(self, cloudConfig, num_threads=5):
        self._queue = Queue.Queue(0)
        self._num_threads = num_threads
        self._task_threads = []
        self._cloudConfig = cloudConfig

        def worker():
            while 1:
                ctxtTuple = self._queue.get()
                if ctxtTuple is None:
                    self._queue.task_done()
                    break # reached end of queue

                for c in ctxtTuple.actions:

                    LOG.info("%s %s started" % (c, ctxtTuple.instanceId))
                    try:
                        ctxt = Action(self._cloudConfig, ctxtTuple.phase, c)
                        ctxt.apply(ctxtTuple.instanceId)
                        LOG.info("%s %s finished" % (c, ctxtTuple.instanceId))
                    except Exception as e:
                        LOG.info(str(e))
                self._queue.task_done()

        for i in range(num_threads):
            t = threading.Thread(target=worker)
            self._task_threads.append(t)
            t.start()

    def submit(self, id_, phase, actions):
        """
        Submits the tasks to the internal FIFO queue for execution.
        """
        self._queue.put(self.__class__.ActionTuple(id_, phase, actions))

    def join(self):
        """
        Blocks until all items in the internal FIFO queue have been gotten and processed.

        """

        for i in range(self._num_threads):
            self._queue.put(None)
        self._queue.join()
        
        


        

    
           
        


"""
.. module:: __init__
   :platform: Unix, Mac
   :synopsis:  This module contains the entry point for starting frieda


.. moduleauthor:: Val Hendrix <vchendrix@lbl.gov>


"""
from __init__ import *

if __name__ == '__main__':
    main()


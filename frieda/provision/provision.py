'''
Created on Jan 31, 2013

@author: val
'''
import logging

from libcloud.compute.base import NodeSize, StorageVolume
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.common.openstack import *
import libcloud

import frieda.util.config as config
import frieda.util.cloudutil as cloudutil
import commands
import json


FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
LOG = logging.getLogger(__name__)


class OpenStackVolumeConnection(OpenStackBaseConnection):
    """
    
    """
    service_type = 'volume'
    service_name = 'cinder'
    service_region = 'RegionOne'


    def _user_agent(self):
        return 'libcloud/%s (%s)%s' % (
            libcloud.__version__,
            "FRIEDA-integration",
            "".join([" (%s)" % x for x in self.ua]))

    libcloud.common.base.Connection._user_agent = _user_agent

    def request(self, action, params=None, data='', headers=None,
                method='GET'):

        # HACK - Setting expected instance variables
        self.accept_format = 'application/json'
        self.default_content_type = 'application/json'

        if not headers:
            headers = {}
        if not params:
            params = {}

        if method in ("POST", "PUT"):
            headers = {'Content-Type': self.default_content_type}

        if method == "GET":
            self._add_cache_busting_to_params(params)

        return super(OpenStackVolumeConnection, self).request(
            action=action,
            params=params, data=data,
            method=method, headers=headers)


def launchNodes(conn, cloud_params, node_reqs):
    """
       Launches nodes
       
        :param conn: libcloud node driver
        :type conn: libcloud.compute.drivers.EC2NodeDriver
        :param cloud_params: The cloud provider configuration
        :type cloud_params: dict
        :param node_reqs: node requirements
        :type node_reqs: list
        :return: instanceIds
        :rtype: list of strings
        
    """
    image = cloudutil.getImage(cloud_params['image_id'], conn)
    #instance_type= cloudutil.getInstanceType (cloud_params['instance_type'], conn)
    instance_type = NodeSize(id=cloud_params['instance_type'], name="", ram=None, disk=50, bandwidth=None, price=None, driver=cloudutil.getDriver(cloud_params))
    
    instanceIds = cloudutil.startInstances(conn, 
                                           image,
                                           instance_type,
                                           cloud_params['keypair'],
                                           node_reqs, 
                                           cloud_params['security_group'])
    return instanceIds


def createVolumes(cloud_config, roleName, instanceCount):
    role = cloud_config.role(roleName)
    storageProv = cloud_config.provision('storage')
    cloud_params = cloud_config.cloud()
    conn = cloudutil.getConnection(cloud_params)

    if 'storage' in role and storageProv:
        storageNames = role['storage']
        if not isinstance(storageNames, list):
            storageNames = [storageNames]
        for storageName in storageNames:
            storage = storageProv[storageName]
            if storage['type'] == 'block' and not 'id' in storage:
                storage['id'] = []
                for i in range(instanceCount):
                    volume = __createVolume(conn, int(storage['size']))
                    storage['id'].append(volume.id)
                    LOG.info("%d - Creating volume:(%s) size:(%s)GB for role:(%s) and storageName:(%s)" % (
                        i, volume.id, volume.size, role, storageName))


def getVolume(conn, cloud_config, roleName):
    """
    Get the volumes for the given role
    """
    role = cloud_config.role(roleName)
    storageProv = cloud_config.provision('storage')
    volume = None
    LOG.info("Getting volume for role:(%s) and storageProv:(%s)" % (roleName,storageProv))
    if 'storage' in role and storageProv:
        storageNames = role['storage']
        if not isinstance(storageNames, list):
            storageNames = [storageNames]
        for storageName in storageNames:
            LOG.info("Getting volume for role:(%s) and storageName:(%s)" % (roleName,storageName))
            storage = storageProv[storageName]
            if storage['type'] == 'block':
                if storage.has_key('id'):
                    if type(storage['id']) == list:
                        for s in storage['id']:
                            volume = StorageVolume(s, storageName, None, conn)
                            storage['id'].remove(s)
                            LOG.info("Getting volume:(%s) size:(%s)GB for role:(%s) and storageName:(%s)" % (
                                volume.id, volume.size, roleName, storageName))

                            break
                    else:
                        volume = StorageVolume(storage['id'], storageName, None, conn)
                        LOG.info("Getting volume:(%s) size:(%s)GB for role:(%s) and storageName:(%s)" % (
                            volume.id, volume.size, roleName, storageName))
                        storage['id'] = None

    return volume    
    
def attachVolume(conn, instance, volume, device):
    """
    Attach volume to a server

    :param conn: The driver for the cloud provider
    :param instance:
    :param volume:
    :param device:
    :return:
    """
    attach = False
    try:
        if isinstance(conn, get_driver(Provider.OPENSTACK)):
            # HACK!! Using nova tools
            attach_command = "nova volume-attach %(server)s %(volume)s %(device)s" % {'server':instance.id, 'volume': volume.id, 'device': device}
            LOG.info(attach_command)
            status, output = commands.getstatusoutput(attach_command)
            if status > 0:
                LOG.error("command '%s' failed  with status:%s output:%s" % (attach_command, status, output))
            else:
                LOG.info("Attached volume: %s" % output)
                attach = True
        else:
            attach = conn.attach_volume(instance, volume, device)
    except Exception, e:
        LOG.error("Attaching volume " + str(e))
        
    return attach


def __createVolume(conn, size):
    #HACK!!!

    """

    :param conn:
    :param size:
    :return:
    """
    volume = None
    if isinstance(conn, get_driver(Provider.OPENSTACK)):

        from  urlparse import urlparse
        url_parts = urlparse(conn._ex_force_auth_url)
        vConn = OpenStackVolumeConnection(user_id=conn.key, key=conn.secret,
                                          secure=False,
                                          host=url_parts.hostname,
                                          port=8776,
                                          ex_tenant_name=conn._ex_tenant_name,
                                          ex_force_auth_url=conn._ex_force_auth_url,
                                          ex_force_auth_version='2.0_password')
        data = {'volume': {
            "size": size,
            "metadata": {}}
        }
        volume_str =vConn.request(action='/volumes',params=None,data=json.dumps(data),headers=None,method='POST').object

        if volume_str:

            volume_json = json.loads(volume_str)['volume']
            volume = StorageVolume(id=volume_json['id'], name=volume_json['display_name'], size=volume_json['size'],
                                   driver=conn)

    else:
        params = {
            'Action': 'CreateVolume',
            'Size': str(size)}

        volume = conn._to_volume(
            conn.connection.request(conn.path, params=params).object,
            name=conn)
    return volume


if __name__ == "__main__":
    config.YAML_CONFIG_TEMPLATE = """
role:
    test: 
        unique: True   
        storage: [test_block,test_new]   
        actions: [os-setup,mount]
orchestration: 
    actions: [test]
    
provision:
    storage:
        test_block: 
            type: block
            id: vol-00000038
            device: /dev/vdc
        test_new:
            type: block
            device: /dev/vdc
            size: 50
    """
    cloud_config = config.CloudConfig(yamlFile="simple-os-futuregrid.yaml")
    cloud_params = cloud_config.cloud()
    conn = cloudutil.getConnection(cloud_params)
    vol = __createVolume(conn, 5)
    print vol


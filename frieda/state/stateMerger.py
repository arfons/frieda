import os
import commands
import pdb
import sys
from pycassa.types import *
from pycassa.system_manager import *
import time

recordSeparator = '|:|'
timeSeparator = '::'


def readDir(path):
	streamList = []
	fList = commands.getstatusoutput('ls ' + path)[1].split()
	i = len(fList)
	for item in fList:
		item = path + item
		stream = open(item, 'r')
		streamList.append(stream)
	return streamList


def merge(srcPath):
	#pdb.set_trace()
	mergedStack = []
	streamList = readDir(srcPath)
	numFiles = len(streamList)
	#groups = []
	dataList = []
	for s in streamList:
		dataList.append(s.readlines())
	#load data ready
	minMC = 9999999999999999

	minIndex = 0
	
	top = ''
	push = ''
	while len(dataList) != 0:
		i=0 #index for stack
		if len(dataList[0])==0:
			del(dataList[0])
			break
		minMC = int(dataList[0][0].split(recordSeparator)[2])
		for stack in dataList:
			if len(stack) == 0:
				del(dataList[i])
				break
			#pdb.set_trace()
			#top = stack[0]
			while len(stack[0].split(recordSeparator)) !=5:
				print 'Found broken line, skipped: ', stack[0]
				del(stack[0]) 
			if int(stack[0].split(recordSeparator)[2]) <= int(minMC):
				minMC = int(stack[0].split(recordSeparator)[2])
				minIndex = i
				push = stack[0]
			if i == len(dataList) - 1:
				mergedStack.append(push)
				del(dataList[minIndex][0])
				
				numLine = len(dataList[minIndex])
				if numLine == 0:#no more lines
					del(dataList[minIndex]) #remove empty stack
				else:
					#pdb.set_trace()
					#print 'push: ', push
					#print 'dataList[minIndex][0]: ', dataList[minIndex][0]
					while 0 < len(dataList[minIndex]) and int(push.split(recordSeparator)[2]) == \
						int(dataList[minIndex][0].split(recordSeparator)[2]):
						#check master clock
						mergedStack.append(dataList[minIndex][0])
						del(dataList[minIndex][0])
			i = i + 1
	for s in streamList:
		s.close()
	return mergedStack			

def mergeToFile(srcPath, targetFile):
	start = round(time.time() * 1000)
	mList = merge(srcPath)
	end = round(time.time() * 1000)
	stream = open(targetFile, 'wa')
	for line in mList:
		stream.write('%s' % line)
	stream.close()
	print 'Merging completed in ' + str(end-start) + ' milliseconds.'

class DBClient:
	hostname='localhost'
	ip=''
	port=9160
	keySpace=''
	pool=None
	columnFamily=None
	columnFamilyName='ColumnFamily1'
	host=''# in terms of hostname:port form
	def __init__(self, hostname, ip, port):
		self.hostname = hostname
		self.ip = ip
		self.port = port
		self.keySpace ='keySpace_'+ hostname

		self.host = ip + ':' + str(port)
		sys = SystemManager(self.host)
		keySet = CompositeType(AsciiType(),AsciiType(), \
			IntegerType(), IntegerType(), IntegerType(), IntegerType(),IntegerType(), IntegerType())
		# format: role::hostName::year::month::day::hour::mintue::sec::eventName
		# sample key: worker::t1::2013::8::8::14::25::9::worker_end

		try:
		    sys.create_keyspace(self.keySpace, strategy_options={"replication_factor": "1"})
		except Exception:
			print "Create: Key space exists."
		try:
			sys.create_column_family(self.keySpace, self.columnFamilyName, comparator_type=self.keySet)
		except Exception:
			print "Create: column_family exists."
		self.pool = ConnectionPool(self.keySpace, [self.host])
		self.columnFamily = ColumnFamily(self.pool, self.columnFamilyName)
	def cassandraPut(self, key, value):
		self.columnFamily.insert()
		#self.columnFamily.insert(key, {'event' : value}, timestamp=time.time() )


	def prepareDB(self, port):
		host = 'localhost' + ':' + str(port)
		sys = SystemManager(host)
		keySet = CompositeType(AsciiType(),AsciiType(), \
			IntegerType(), IntegerType(), IntegerType(), IntegerType(),IntegerType(), IntegerType())
		# sample key: worker::t1::2013::8::8::14::25::9::worker_end





def mergeToDB():
	pass

	
#print merge('../merge_states/sample/')
					
#mergeToFile('/Users/tony/sample/new2/','/Users/tony/sample/new2/merged.dat')
mergeToFile(sys.argv[1], sys.argv[2])




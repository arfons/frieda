"""
State Management System for FRIEDA
Author: Tonglin Li
Email: tli13@hawk.iit.edu 

"""

import pycassa
from pycassa.pool import *
from pycassa.system_manager import *
from pycassa.columnfamily import *
import socket
import time
import vectorClock
from datetime import timedelta, datetime
import yaml
import json
import commands
#Cassandra put method wrapper
import os
import socket

import boto.dynamodb2
from boto.dynamodb2.fields import HashKey, RangeKey, KeysOnlyIndex, AllIndex
from boto.dynamodb2.table import Table
from boto.dynamodb2.types import NUMBER

if os.name != "nt":
    import fcntl
    import struct

    def getInterfaceIP(ifname):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s',
                                ifname[:15]))[20:24])

def getLanIP():
    try:
        ip = socket.gethostbyname(socket.gethostname())
    except:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("gmail.com",80))
        ip = s.getsockname()[0]
        s.close()

    if ip.startswith("127.") and os.name != "nt":
        interfaces = [
            "eth0",
            "eth1",
            "eth2",
            "wlan0",
            "wlan1",
            "wifi0",
            "ath0",
            "ath1",
            "ppp0",
            ]
        for ifname in interfaces:
            try:
                ip = getInterfaceIP(ifname)
                break
            except IOError:
                pass
    return ip



class cassandraClient:
	hostname='localhost'
	ip=''
	port=9160
	keySpace=''
	pool=None
	columnFamily=None
	host=''# in terms of hostname:port form
	def __init__(self, hostname, ip, port):
		self.hostname = hostname
		self.ip = ip
		self.port = port
		self.keySpace ='keySpace_states' # + hostname
		self.host = ip + ':' + str(port)
		sys = SystemManager(self.host)
		try:
		    sys.create_keyspace(self.keySpace, strategy_options={"replication_factor": "1"})
		except Exception:
			print "Create: Key space exists."
		try:
			sys.create_column_family(self.keySpace, 'ColumnFamily1')
		except Exception:
			print "Create: column_family exists."
		self.pool = ConnectionPool(self.keySpace, [self.host])
		self.columnFamily = ColumnFamily(self.pool, 'ColumnFamily1')

	def cassandraGet(self, key):
		return self.columnFamily.get(key)

	def cassandraPut(self, key, value):
#		self.columnFamily.insert(key, {value : str(datetime.now())} ) #str(datetime.now())
		self.columnFamily.insert(key, {'event' : value}, timestamp=time.time() ) #str(datetime.now())
	
	def cassandraGetBlob(self, key):
		return self.columnFamily.get(key)

	def listAll(self):
		return self.columnFamily.get_range(column_count=0, filter_empty=False)
		#colFam = ColumnFamily(self.pool, 'ColumnFamily1')
	#only for single node clint test, not for aggregated data use!!!	
	def close(self):
		pass
	
		
class fileRecordClient:
	filePath=''
	fileStream=None
	dataFile=None
	separator=' '
	def __init__(self, filePath, separator):
		self.filePath=filePath
		bufsize = 100000000
		self.fileStream=open(self.filePath, 'w+', bufsize)
		self.separator=str(separator)

	def put(self, key, value):		
		timestamp=str(datetime.now()) #str(time.time())
		dataStr=key+self.separator+value+self.separator+timestamp+'\n'
		self.fileStream.write(dataStr)


	def close(self):
		self.fileStream.close()	

class dynamoDBClient:
	tableName=''
	table=None
	readMax=50
	writeMax=100

	def __init__(self, tableName, readMax, writeMax):
		print 'tableName: ', tableName
		self.tableName = tableName
		self.readMax = readMax
		self.writeMax = writeMax
		try:
			self.table = Table(tableName)
			self.table.count()
		except Exception:
			print "Table not found, will create one for you ..."
			self.table = self.createTable(tableName)

	def createTable(self, tableName):
		#print tableName
		self.table = Table.create(tableName, schema=[ HashKey('key'),],\
		  throughput = {'read': self.readMax,'write': self.writeMax,})

	def dynamodbPut(self, key, value):
		try:
			#self.table.put_item(data = {'key': key, 'value': value, })
			print 'key=', key
			self.table.put_item(data = {'key': key, 'value': value, 'timestamp': str(datetime.now()),})
		except Exception:
			print 'Key conflict, put failed.'	

	def dynamodbGet(self, searchKey):
		return self.table.get_item(key = searchKey)['value']

	def close(self):
		pass

class stateManagerClient:
	
	storeType=''
	storeClient=None
	hostname=''
	ip=''
	port=0
	role='' #master/controller/worker
	separator=' '
	keySeparator='::'
	def __init__(self, role, storeType, pathPrefix, hostname, ip, port, separator):
		self.storeType = storeType
		self.role = role
		self.hostname = hostname
		self.ip = ip
		self.port = port
		if storeType == 'Cassandra':
			self.storeClient = cassandraClient(self.hostname, self.ip, str(self.port))
		if self.storeType=='file':
			path=os.path.join(pathPrefix, role + '_' + self.hostname +'_states.dat')
			self.separator=separator
			self.storeClient = fileRecordClient(path, self.separator)
		if self.storeType=='DynamoDB':
			tableName='FRIEDA_STATES_DB1'
			readMax=10
			writeMax=100
			self.storeClient = dynamoDBClient(tableName, readMax, writeMax)

	def put(self, key, value, vectorClock):
		vectorClock.updateLocalClock()
		now=datetime.now()

		dateKeys=str(now.year)+self.keySeparator+str(now.month)+self.keySeparator+ \
			str(now.day)+self.keySeparator+str(now.hour)+self.keySeparator+ \
			str(now.minute)+self.keySeparator+str(now.second)
		storedKey = str(self.role) + self.keySeparator + self.hostname + \
			self.keySeparator + dateKeys + self.keySeparator + key
		if self.storeType=='Cassandra':
			self.storeClient.cassandraPut(storedKey, str(value))
		if self.storeType=='file':
			self.storeClient.put(storedKey, str(value) + self.separator + 
				str(vectorClock.masterClock) + self.separator + str(vectorClock.localClock))
		if self.storeType=='DynamoDB':
			self.storeClient.dynamodbPut(storedKey, str(value))
	#only for single node clint test, not for aggregated data use!!!
	
	def get(self, key):
		storedKey = str(self.role) + '-' + self.hostname + '-' + key
		if self.storeType=='Cassandra':
			return self.storeClient.cassandraGet(storedKey)
		if self.storeType=='DynamoDB':
			return self.storeClient.dynamodbGet(storedKey)

	def putYaml(self, key, yamlFile):
		stream = open(yamlFile,'r')
		dataMap = yaml.load(stream)
		stream.close()
		dataStr= json.dumps(dataMap)
		
		if self.storeType=='Cassandra':
			self.storeClient.cassandraPutBlob(key, dataStr)
		if self.storeType=='file':
			self.storeClient.put(key, dataStr)

	def getDict(self, key):
		dataStr = self.get(key)
		dic = json.loads(dataStr['event'])
		return dic

	def runCommand(self, key, cmd, vectorClock):
		ret = str(commands.getstatusoutput(cmd))
		self.put(key, ret, vectorClock)

	def listAll(self):
		return self.storeClient.listAll()

	def storeClose(self):
		self.storeClient.close()


#test case ...
'''
print 'Cassandra API wrapper test case running...'
manager = stateManagerClient('tester', 'Cassandra', 'localhost','127.0.0.1', 9160)
manager.put('myKey','myValue') #'myKey','myValue'
print manager.get('tester-localhost-myKey')
'''


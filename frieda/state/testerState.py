from statesManager import *
from vectorClock import *
#from frieda.state.vectorClock import
import string
import random
import time
import sys

def randomStringGen(length):
	charSet = string.letters + string.digits
	return ''.join(random.choice(charSet) for x in range(length))



separator ='|:|'
role = 'tester'

def benchmark(storeType, ip, ops, kLen, vLen):
	manager = stateManagerClient(role, storeType, os.environ['PWD'], socket.gethostname(), ip, 9160, separator) #getLanIP()
	global VECTOR_CLOCK
	VECTOR_CLOCK = VectorClock(separator)
	VECTOR_CLOCK.updateMasterClockByLocal()
	start = time.time()*1000 # in ms
	manager.put('Start','time', VECTOR_CLOCK)
	latencyList = []
	sigma=0
	for i in range(0, int(ops)):
		s = time.time()*1000*1000 # in microsec
		manager.put(randomStringGen(int(kLen)),randomStringGen(int(vLen)), VECTOR_CLOCK)
		e = time.time()*1000*1000
		delta = e - s
		latencyList.append( delta )
		sigma = delta + sigma
	manager.put('End','time', VECTOR_CLOCK)
	manager.storeClose()
	end = time.time()*1000
	print 'writing ', str(ops) ,' state records to file cost', str(end -  start), ' ms'
	print 'Average latency = ', sigma/int(ops), 'ns' 
	return latencyList

def saveListToFile(latencyList, path):
	bufsize = 5000000
	fileStream=open(path, 'w+', bufsize)
	for item in latencyList:
		fileStream.write(str(item)+'\n')

	fileStream.close()
	
def run(storeType, ip, ops, kLen, vLen, latencyFilePath):
	saveListToFile(benchmark(storeType, ip, ops, kLen, vLen), latencyFilePath)

run(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])


"""
.. module:: cloudutil
   :platform: Unix, Mac
   :synopsis:  This module contains the utilites for configuration clouds


.. moduleauthor:: Val Hendrix <vchendrix@lbl.gov>


"""
import commands
import os
import logging
import time
import paramiko
import sys
from libcloud.compute.drivers.ec2 import EucNodeDriver
from libcloud.compute.drivers.ec2 import EC2NodeDriver
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.ssh import ParamikoSSHClient
from libcloud.compute.types import NodeState
from urlparse import urlparse

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
LOG = logging.getLogger(__name__)

NODE_STATE = ['RUNNING', 'REBOOTING', 'TERMINATED', 'PENDING', 'UNKNOWN']


class CloudUtilException(Exception):
    pass

class IPConnection():
    pass
class IPDriver():
    def __init__(self,name):
        self.name=name
    def list_nodes(self):
        ids=self.name['instance_id'].split(',')
        nodes=[]
        for ip in ids:
            ip_node=IPNode(ip)
            ip_node.id=ip
            nodes.append(ip_node)
        return nodes

class IPNode():
    id = None
    state=NodeState.RUNNING
    def __init__(self,name):
        self.name=name
        self.public_ips=[name.split('@')[1],]
        self.private_ips=[name.split('@')[1],]


def isUnknown(n):
    """
    Check the state of the node to determine if it is UNKNOWN
    :param n: The node to test
    :type n: libcloud.compute.Node
    :return: Bool
    """
    return n.state == NodeState.UNKNOWN


def isReady(n, cloud_params):
    """
    Determine if the node is ready for preparation.  Ready for this action means that it has
        and ip address, is running and can be connected to via ssh
    :param n: The node to test
    :type n: libcloud.compute.Node
    :return: Bool
    """
    has_interface = False
    is_running = False
    has_connection = False
    if n:
        is_running = n.state == NodeState.RUNNING

        ssh_interface = 'public_ips'
        if 'ssh_interface' in cloud_params:
            ssh_interface = cloud_params['ssh_interface']
        addresses = getattr(n, ssh_interface) 
        has_interface = addresses and len(addresses) > 0
        has_connection = False
        if cloud_params['platform']=='ip_address':
            if has_interface:
                ssh_keys = [os.path.expanduser("~/.ssh/%s.pem" % cloud_params['keypair'])]
                print addresses
                print n.id.split('@')[0]
                print ssh_keys
                sshConn = ParamikoSSHClient(addresses[len(addresses) - 1], port=22, username=n.id.split('@')[0],
                                            password=None,
                                            key=ssh_keys, timeout=None)
                LOG.debug("isReady(%(id)s), SSH connect %(ip)s %(username)s %(key)s" % {'id': n.id,
                                                                                        'ip': addresses[len(addresses) - 1],
                                                                                        'username': n.id.split('@')[0],
                                                                                        'key': str(ssh_keys)})
                try:
                    sshConn.connect()
                    has_connection = True
                except Exception, e:
                    print e
                    LOG.debug("isReady(%s), SSH connect" % n.id, e)
                    pass
        else:
            if has_interface:
                ssh_keys = [os.path.expanduser("~/.ssh/%s.pem" % cloud_params['keypair'])]
                sshConn = ParamikoSSHClient(addresses[len(addresses) - 1], port=22, username=cloud_params['ssh_user'],
                                            password=None,
                                            key=ssh_keys, timeout=None)
                LOG.debug("isReady(%(id)s), SSH connect %(ip)s %(username)s %(key)s" % {'id': n.id,
                                                                                        'ip': addresses[len(addresses) - 1],
                                                                                        'username': cloud_params[
                                                                                            'ssh_user'],
                                                                                        'key': str(ssh_keys)})
                try:
                    sshConn.connect()
                    has_connection = True
                except Exception, e:
                    LOG.debug("isReady(%s), SSH connect" % n.id, str(e))
                    pass
            sshConn.close()
    return has_interface and is_running and has_connection


def getDriver(cloud_params):
    """
        Get the libcloud Driver
        :param cloud_params: The cloud provider configuration
        :type cloud_config: dict
    """
    if cloud_params['platform'] == 'ec2':
        return get_driver(Provider.EC2)
    elif cloud_params['platform'] == 'openstack':
        return get_driver(Provider.OPENSTACK)
    elif cloud_params['platform'] == 'euca':
        return get_driver(Provider.EUCALYPTUS)
    elif cloud_params['platform'] == 'ip_address':
        return None
    else:
        raise NotImplementedError("No support for %s" % cloud_params['platform'])


def validateEnvironmentVariables(environment_vars):
    """

    Validate that the specificed environment variables exist

    :param environment_vars: required environment variables
    :type environment_vars: set
    :return:
    """
    found = environment_vars.intersection(os.environ.keys())
    if len(found) != len(environment_vars):
        raise CloudUtilException(
            "Missing Environment Variables! required variables are %s on found %s" % (environment_vars, found))
    
def getConnection(cloud_params):
    """
        Get the libcloud connection
        :param cloud_params: The cloud provider configuration
        :type cloud_params: dict
    """
    Driver = getDriver(cloud_params)
    conn = None
    if Driver == EC2NodeDriver or Driver == EucNodeDriver:
        validateEnvironmentVariables(set(['EC2_URL', 'EC2_ACCESS_KEY', 'EC2_SECRET_KEY']))

        url_parts = urlparse(os.environ['EC2_URL'])
        secure = False
        if url_parts.scheme == 'https':
            secure = True
            
        conn = Driver(os.environ['EC2_ACCESS_KEY'],
                      os.environ['EC2_SECRET_KEY'],
                      secure=secure,
                      host=url_parts.hostname,
                      port=url_parts.port,
                      path=url_parts.path)

    elif cloud_params['platform'] == 'openstack':
        validateEnvironmentVariables(set(['OS_USERNAME', 'OS_PASSWORD', 'OS_TENANT_NAME', 'OS_AUTH_URL']))
        service_name = None
        if 'OS_COMPUTE_SERVICE_NAME' in os.environ and os.environ['OS_COMPUTE_SERVICE_NAME']:
            service_name = os.environ['OS_COMPUTE_SERVICE_NAME']

        conn = Driver(key=os.environ['OS_USERNAME'], secret=os.environ['OS_PASSWORD'],
                      ex_tenant_name=os.environ['OS_TENANT_NAME'],
                      ex_force_auth_url=os.environ['OS_AUTH_URL'], ex_force_auth_version='2.0_password',
                      ex_force_service_name=service_name)
    elif cloud_params['platform'] == 'ip_address':
        conn = IPDriver(cloud_params)
    else:
        validateEnvironmentVariables(set(['EC2_ACCESS_KEY', 'EC2_SECRET_KEY']))

        conn = Driver(os.environ['EC2_ACCESS_KEY'],
                      os.environ['EC2_SECRET_KEY'])
    return conn


def getFileContent(filename):
    """
    Returns the content of the given filename
    
    :param filename: the full path of the file to get the content for
    """
    tf = open(os.path.expanduser(filename), 'r')
    templateStr = tf.read()
    return templateStr


def getTemplateFileContent(templateFilename, params):
    """
    Returns the result of the specific parameters applied to the
    given template file.
    
    :param templateFilename: The full path to the template file
    :param params: the paramters to apply to the template
    :type params: dict
    """
    return getFileContent(templateFilename) % params


def getNode(node_id, conn):
    """
    Use the cloud connection to get a specific node
    
    :param node_id: the node to retrieve
    :param conn: The Cloud connection
    
    """
    for i in conn.list_nodes(): # ex_node_ids=[nodeid]):
        #print i.id,":::",node_id
        if i.id == node_id:
            return i

    raise CloudUtilException("Node ID: %s returns None" % node_id)


def getNodesForInstanceIds(ids, conn):
    """
    Use the cloud connection to get the instanceIds prefixed with node_id
    
    :param ids: the ids for the node
    :param conn: The Cloud connection
    """
    instanceIds = [n for n in conn.list_nodes() if n.id in ids]
    return instanceIds


def getNodesForKeyname(keyname, conn):
    """
    Use the cloud connection to get the instanceIds prefixed with node_id
    
    :param keyname: the keyname for the node
    :param conn: The Cloud connection
    
    """
    instanceIds = []
    for i in conn.list_nodes():
        if i.state == NodeState.RUNNING:
            # Serious HACK Openstack uses key_name and ec2 api uses keyname
            if (('keyname' in i.extra and i.extra['keyname'] == keyname) or \
                ('key_name' in i.extra and i.extra['key_name'] == keyname)):
                instanceIds.append(i)
    return instanceIds


def getNodes(node_id, conn):
    """
    Use the cloud connection to get the instanceIds prefixed with node_id
    
    :param node_id: the node to retrieve
    :param conn: The Cloud connection
    
    """
    instanceIds = []
    for i in conn.list_nodes():
        if i.name.startswith(node_id):
            instanceIds.append(i)
    return instanceIds


def getImage(image_id, conn):
    """
    Use the cloud connection to get the image specified by imageid
    
    :param image_id: the image to retrieve
    :param conn: The Cloud connection
    
    """
    for i in conn.list_images():
        if i.id == image_id:
            return i
    raise CloudUtilException("Image ID: %s returns None" % image_id)


def getInstanceType(instance_id, conn):
    """
    Use the cloud connection to get the instace type specified by instanceid
    
    :param instance_id: the instance type to retrieve
    :param conn: The Cloud connection
    
    """
    for i in conn.list_sizes():
        if i.id == instance_id:
            return i
    raise CloudUtilException("Instance ID: %s returns None" % instance_id)


def startInstances(conn, image, instanceType, keyname, names, securityGroup):
    """
    Start instances for the list of names
    
    :param conn: the cloud provider connection 
    :param image: the image id to start the instances with
    :param instanceType: The size of instance to start
    :param keyname: name of the key to apply to the ssh_user on each instance
    :param names: the name of the instances to start
    :type names: list
    :return: instanceIds
    :rtype: list of strings
    """

    instanceIds = []
    print "NAMES: ", names
    for nm in names:
        node = conn.create_node(name=nm, image=image, size=instanceType, ex_keyname=keyname, ex_securitygroup=securityGroup)
        instanceIds.append(node.id)
        LOG.info("Create node %s of type %s" % (nm,instanceType.id))
        time.sleep(45)

    return instanceIds


def catFile(ip_address, user, filename, keys):
    """
    Cats a file on the specified host
    
    :param ip_address:  ip of the host
    :param user: ssh user name
    :param filename: the name of the file to put on the host
    :param keys: the ssh keys
    :type keys: list
    
    
    """
    sshConn = ParamikoSSHClient(ip_address, port=22, username=user, password=None, key=keys, timeout=None)
    if sshConn.connect():
        stdout, stderr, exit_status = sshConn.run("cat %s" % filename)
        sshConn.close()
        return stdout, stderr, exit_status
    return None


def printOutput(outfile, data):
    if outfile:
        outfile.write(data)
    else:
        print data


def makeExecute(commandTemplate):
    """
    Make a function to place a script on an instance and execute it.
    
    :param commandTemplate: The command to run on the instance. A valid template parameter
         %filename)s may be used.
    """

    def makeExecute(ip_address, filename, template, params, ssh_user, ssh_keys, outfile=None):
        putFile(ip_address=ip_address,
                user=ssh_user,
                filename=filename,
                filecontent=getTemplateFileContent(template, params),
                keys=ssh_keys)
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(ip_address, username=ssh_user, key_filename=ssh_keys)
        chan = ssh.get_transport().open_session()
        chan.get_pty()
        chan.exec_command(commandTemplate % {"filename": filename})
        printOutput(outfile, commandTemplate % {"filename": filename})
        data = chan.recv(1024)
        if outfile: outfile.write(data)
        while data:
            data = chan.recv(1024)
            printOutput(outfile, data)
        chan.close()

    return makeExecute


def run(ip_address, ssh_user, ssh_keys, command, outfile=None):
    """
    Run a command on the specified host

    :param ip_address: ip address to connect to
    :param ssh_user: user name for the ssh connection
    :param ssh_keys: keys for the ssh user
    :type ssh_keys: list
    :param command: The command to run on the host
    :param outfile: The file name for the output.
    :return:
    """
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip_address, username=ssh_user, key_filename=ssh_keys)
    chan = ssh.get_transport().open_session()
    chan.get_pty()
    chan.exec_command(command)
    printOutput(outfile, command)
    data = chan.recv(1024)
    if outfile: outfile.write(data)
    while data:
        data = chan.recv(1024)
        printOutput(outfile, data)
    chan.close()


def putSSHKey(ip_address, ssh_user, ssh_keys, user, user_home, pem_file):
    """
    Plate an ssh key for a user on a node
    :param ip_address: ip address to connect to
    :param ssh_user: user name for the ssh connection
    :param ssh_keys: keys for the ssh user
    :type ssh_keys: list
    :param user: the user that owns the key being places
    :param user_home: the home directory of the user
    :param pem_file: the ssh key file to place
    :return:
    """
    ssh_dir = "%s/.ssh" % user_home
    command = "sudo mkdir %s" % (ssh_dir)
    run(ip_address, ssh_user, ssh_keys, command)
    pem_path, pem = os.path.split(pem_file)

    putFile(ip_address=ip_address,
            user=ssh_user,
            filename=pem,
            filecontent=getFileContent(pem_file),
            keys=ssh_keys,
            chmod=0600)

    run(ip_address, ssh_user, ssh_keys,
        "sudo mv /tmp/%s %s" % (pem, ssh_dir))
    run(ip_address, ssh_user, ssh_keys,
        "sudo chown -R %s:%s %s" % (user, user, ssh_dir))


def putFile(ip_address, user, filename, filecontent, keys, filepath="/tmp", chmod=0777):
    """
    Place a file on the instance via scp.
    
    :param ip_address: ip of the host
    :param user: ssh user name
    :param filename: the name of the file to put on the host
    :param filecontent: the contents of the file
    :param keys: the ssh keys
    :type keys: list
    :param filepath: the directory where the file is placed
    """
    sshConn = ParamikoSSHClient(ip_address, port=22, username=user, password=None, key=keys, timeout=None)
    if sshConn.connect():
        sshConn.put("%s/%s" % (filepath, filename), contents=filecontent, chmod=chmod, mode='w')
        sshConn.close()
        return True
    return False


def scpFile(ip_address, user, key, source, target):
    """
    Security copy a local file to a host
    :param ip_address: ip of the host
    :param user: ssh user name
    :param key: the ssh key for connecting to the host
    :param source: file to copy
    :param target: copied file at the target
    :return:
    """
    rsyncCommand = "scp -oUserKnownHostsFile=no -oStrictHostKeyChecking=no -i %s %s %s@%s:%s" % (
        key, source, user, ip_address, target)

    status, output = commands.getstatusoutput(rsyncCommand)

    if status > 0:
        raise Exception("scp command failed  with status:%s output:%s" % (status, output))


def rsyncDir(ip_address, user, key, source, target):
    """
    rsync a directory to to a host

    :param ip_address: ip of the host
    :param user: ssh user name
    :param key: the ssh key for connecting to the host
    :param source: directory to copy
    :param target: copied directory at the target
    :return:
    """
    rsyncCommand = "rsync -auvz -e 'ssh -oUserKnownHostsFile=no -oStrictHostKeyChecking=no -i %s' %s %s@%s:%s" % (
        key, source, user, ip_address, target)

    status, output = commands.getstatusoutput(rsyncCommand)

    if status > 0:
        raise Exception("rsync command failed  with status:%s output:%s" % (status, output))
    LOG.info(status)


# Make some functions for executing commands
executeScript = makeExecute("sudo bash /tmp/%(filename)s ")
applyPuppet = makeExecute("sudo puppet apply /tmp/%(filename)s")



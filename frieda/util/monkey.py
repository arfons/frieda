'''

@author: val




'''
from collections import deque
import logging
import sys
import time
import os
from frieda.preparation.preparation import InstancePreparator
from frieda.provision.provision import createVolumes, getVolume, launchNodes, attachVolume
import frieda.util.cloudutil as cloudutil
from frieda.util.cloudutil import NODE_STATE
import frieda.util.config as config
import ast
import xmlrpclib
import re
import pdb

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
LOG = logging.getLogger(__name__)



class SafeTransportWithCert(xmlrpclib.SafeTransport): 
     __cert_file = ""
     __key_file = ""
     _use_datetime = False

     def __init__(self, certFile, keyFile):
         self.__cert_file = certFile
         self.__key_file = keyFile
         
     def make_connection(self,host): 
         host_with_cert = (host, { 
                       'key_file'  :  self.__key_file, 
                       'cert_file' :  self.__cert_file 
             } ) 
         return  xmlrpclib.SafeTransport.make_connection(self,host_with_cert) 



def addToEnviron(roleName, role, instance):
    """
    Add the node information to the environment
    
    :param roleName: The name of the role
    :param role: the role 
    :type role: dict
    :param instance:
    """

    id_field = "%s_ID" % roleName.upper()
    ip_field = "%s_IP" % roleName.upper()
    private_ip_field = "%s_PRIVATE_IP" % roleName.upper()
    if role['unique']:
        os.environ[id_field] = instance.id
        if instance.public_ips and len(instance.public_ips) > 0:
            os.environ[ip_field] = instance.public_ips[0]
        os.environ[private_ip_field] = instance.private_ips[-1]
    else:
        id_field="%sS" % id_field
        ip_field="%sS" % ip_field
        private_ip_field="%sS" % private_ip_field
        if id_field not in os.environ:
            os.environ[id_field] = ""
            os.environ[ip_field] = ""
            os.environ[private_ip_field] = ""
        os.environ[id_field] = "%s %s" % (os.environ[id_field], instance.id)
        if instance.public_ips and len(instance.public_ips) > 0:
            ### Bug: In case of futuregrid Openstack,
            ### instance.public_ips[0] always return empty list. should use instance.private_ips[-1]
            os.environ[ip_field] = "%s %s" % (os.environ[ip_field], instance.public_ips[0])
        os.environ[private_ip_field] = "%s %s" % (os.environ[private_ip_field], instance.private_ips[0])


def makeForEach(list_):
    """
    Makes a function that iterates over a list

    :param list_:
    :return: function
    """
    def forEachNode(fn, conn=None):
        for l in list_:
            fn(l, conn)

    return forEachNode


def destroyNode(n, conn=None):
    """
    Destroys a node
    :param n: the node to destroy
    :type n: libcloud.compute.Node
    """
    if n:
        LOG.info("destroying node %s\t%s\t%s\t%s\t" % ("", n.id, n.public_ips, n.private_ips))
        conn.destroy_node(n)


def listNode(n, conn=None):
    """
    Prints the information of a node to the console
    :param n: the node to destroy
    :type n: libcloud.compute.Node
    """
    if n:
        print "%s\t%s\t%s\t%s\t" % (n.id, NODE_STATE[n.state], n.public_ips, n.private_ips)


def printList(list_):
    """
    print a space delimited list to the console

    :param list_: the list to print
    :type list_: list
    :return:
    """
    print "Nodes:", str(list_).strip("[]'").replace("', '", " ")


def writeInstanceIdsToEnv(instances, f, role, unique):
    """
    Write the instances information for the specified role to the given file
    :param instances: A dict of instances
    :type instances: dict
    :param f:
    :param role: role 
    :type role: str
    :return:
    """
    if unique:
        i = instances[0]
        f.write('export %s_ID=%s\n' % (role.upper(), str(i[0])))
        os.environ['%s_ID' % (role.upper())] = str(i[0])
        f.write('export %s_IP=%s\n' % (role.upper(), str(i[1])))
        os.environ['%s_IP' % (role.upper())] = str(i[1])
        f.write('export %s_PRIVATE_IP=%s\n' % (role.upper(), str(i[2]) ))
        os.environ['%s_PRIVATE_IP' % (role.upper())] = str(i[2])
        f.write('export %s_VOLUME=%s\n' % (role.upper(), i[3]))
        os.environ['%s_VOLUME' % (role.upper())] = i[3]
    else:
        volumes= str([i[3] for i in instances]).strip("[]'").replace("', '", " ")
        f.write('export %s_VOLUMES="%s"\n' % (
            role.upper(), volumes ))
        os.environ['%s_VOLUMES' % (role.upper())] =  volumes


        ids=str([i[0] for i in instances]).strip("[]'").replace("', '", " ")
        f.write(
            'export %s_IDS="%s"\n' % (role.upper(), ids))
        os.environ['%s_IDS' % (role.upper())] =  ids

        ips=str([ str(i[1]) for i in instances]).strip('[]').replace("'", "").replace(" ", "")
        f.write('export %s_IPS=%s\n' % (
            role.upper(), ips ))
        os.environ['%s_IPS' % (role.upper())] =  ips
        private_ips=str([  str(i[2]) for i in instances]).strip('[]').replace("'", "").replace(" ", "")
        f.write('export %s_PRIVATE_IPS=%s\n' % (
            role.upper(), private_ips))
        os.environ['%s_PRIVATE_IPS' % (role.upper())] =  private_ips

def writeDatacoordinatorToEnv(dcn,f):
    """
    Write Data Coordinator details to a file

    """
    f.write('export DCN_HOST=%s\n' % (dcn['host']));
    f.write('export DCN_PORT=%s\n' % (dcn['port']));
    f.write('export DCN_PROTOCOL=%s\n' % (dcn['protocol']));
    
def write_scale_envfile(instance_names, config_file):
     config_dir = os.path.split(config_file)[0]
     env_file = "%s/appenv.scale" %(config_dir)

     cloud_config = config.CloudConfig(yamlFile=config_file)

     instances = []
     #instance_ids = instance_names.split(',') 
     
     #writeEnvFile(cloud_config, open(env_file, 'w'))
     conn = cloudutil.getConnection(cloud_config.cloud())
     for instance_id in instance_names.split(' '):
          instance = cloudutil.getNode(instance_id, conn)
          if cloudutil.isReady(instance, cloud_config.cloud()):
               #addToEnviron(roleName, role, instance)
               volume = getVolume(conn, cloud_config, 'worker')
               if volume:
                    attachVolume(conn, instance, volume, device)
               volumeId = ""
               if volume:
                    volumeId = volume.id
               public_ip = None
               if instance.public_ips and len(instance.public_ips) > 0:
                    public_ip = instance.public_ips[0]
          instances.append([instance.id, public_ip, instance.private_ips[0], volumeId])

     writeInstanceIdsToEnv(instances, open(env_file, 'w'), 'worker', False)
    
def writeEnvFile(cloud_config, f):
    """
    Write the cluster instance information to a file

    :param cloud_config: The cloud configurations
    :type cloud_config:  config.CloudConfig
    :param f: The file to write to
    :type f: FileIO
    :return: None
    """
    for roleName in cloud_config.orchestration('roles'):
        role = cloud_config.role(roleName)
        instances = role['instances']
        #print "Instances: %s" %(instances)
        #sys.exit()
        unique = role['unique']
        writeInstanceIdsToEnv(instances, f, roleName, unique)

    ### THIS IS PROBABLY NOT USED BECAUSE friedamode is never datacoordinator###
    #dcn={} 
    #friedamode = cloud_config.frieda('mode')
    #if(friedamode == 'datacoordinator'):
    #    dcn['host'] = cloud_config.frieda('dcn_host')
    #    dcn['port'] = cloud_config.frieda('dcn_port')
    #    dcn['protocol'] = cloud_config.frieda('dcn_protocol')
    #    print dcn
    #    writeDatacoordinatorToEnv(dcn,f)
    f.close()

def getServer(args):
    """ Get Server details """

    server_url = args['server']
    print "Contacting ORCA xml-rpc server " + server_url + " for creating the \
sliver... \n"

    cert = args['certificate']
    privateKey = args['private']
    if server_url.startswith('https://'):
        # create secure transport with client cert
        transport = SafeTransportWithCert(cert, privateKey)
        server = xmlrpclib.Server(server_url, transport=transport)
    else:
        server = xmlrpclib.Server(server_url)

    return server


def provisionOrca(cloud_config, server, args, config_file):

    """
    Provision Slice
    :param server: Server object
    :param args: command line arguments
    """
    LOG.info("Provisioning Slice/Cluster...")
    request = args['request']
    f = open( request, 'r' )
    ndlReq = f.read()
    f.close()


    ndlReq=ndlReq.replace( 'NBR_INSTANCES', args['nbr_instances'] )
    
    users = []
    key = args['key']

    f = open(key, 'r')
    userKey = f.read()
    users=[ {'urn':'authorized_user', 'keys': [ userKey ] } ]
    credentials=[]

    result = server.orca.createSlice(args['slice_id'], credentials, ndlReq, users)


    LOG.info("Slice %s Created.. Acquiring IPS of instances..." % args['slice_id'])

    instance_public_ips = []    

    while len(instance_public_ips) != int(args['nbr_instances']):
        delay = 120
        LOG.info("Public IPS of instances not yet ready... Retry after %s seconds" % delay)
        time.sleep(delay)
        manifest = server.orca.sliceStatus(args['slice_id'], credentials)
        instance_public_ips = list(set(re.findall("\d+\.\d+\.\d+\.\d+", str(manifest))))

    os.environ['MASTER_IP'] = instance_public_ips[0]
    LOG.info('Master IP: %s', os.environ['MASTER_IP'] )
    os.environ['WORKER_IPS'] = ",".join(instance_public_ips[1:])
    LOG.info('Worker IPs: %s', os.environ['WORKER_IPS'] )

    writeOrcaEnvFile(config_file)


    phase = "preparation"
    preparation(cloud_config, phase, ",".join(cloud_config.role('master')['actions']), [os.environ['MASTER_IP']])
    preparation(cloud_config, phase, ",".join(cloud_config.role('worker')['actions']), os.environ['WORKER_IPS'].split(","))


def writeOrcaEnvFile(config_file):
    """ Generate Orca Environment file with master and worker IPs """

    yamlFilePath, yamlFileName=os.path.split(config_file)
    envFile = "%s/%s_env" % (yamlFilePath,yamlFileName.split(".")[0] )
    envf = open(envFile,"w")
    envf.write("export MASTER_IP=%s\n" % os.environ['MASTER_IP'])
    envf.write("export WORKER_IPS=%s\n" % os.environ['WORKER_IPS'])
    envf.close()    


def deleteSlice(server, args):
    """
    Delete Slice based on slice id
    :param server: Server object
    :param args: command line arguments
    """

    result = server.orca.deleteSlice(args['slice_id'], [])
    if result['err']:
        LOG.error("Error in deleting slice: %s " % result['msg'])
        sys.exit(0)


def provision(cloud_config, node_names, instanceIds=None):
    """
    Use the cloud configuration to automatically provision and preparation a cluster
    of nodes

    :param cloud_config: The cloud config to preparation
    :type cloud_config: config.CloudConfig
    :param node_names: the name of the nodes to launch
    :type node_names: list
    :param instanceIds: The instance ids of the launched nodes. This is optional. if
        no instance id exists then the nodes are launched
    :return:
    """
    yamlFilePath, yamlFileName=os.path.split(cloud_config.yamlFile)
    #envFile = "%s/%s_env" % (yamlFilePath,yamlFileName.split(".")[0])
    envFile = "%s/appenv" % (yamlFilePath)
    yamlLaunchFile = "%s/applaunch.yaml" % (yamlFilePath)
    preparator = InstancePreparator(cloud_config, num_threads=40)
    startTime = time.time()
    provisionTime = 0
    launchTime = 0
    #try:
    if True:
        if cloud_config.cloud()['platform'] == 'ip_address':
            conn = cloudutil.IPDriver(cloud_config.cloud())
        else:
            conn = cloudutil.getConnection(cloud_config.cloud())
        print "NodeNames: ", node_names, "InstIds: ",instanceIds
        if not instanceIds:
            instanceIds = launchNodes(conn, cloud_config.cloud(), node_names)
            launchTime = time.time() - startTime

        printList(instanceIds)
        roleDeque = deque(cloud_config.orchestration('roles'))
        instanceIdDeque = deque(instanceIds)
        device = None
        if 'device' in cloud_config.cloud():
            device = cloud_config.cloud()['device']
        while len(roleDeque) > 0:
            roleName = roleDeque.popleft()
            role = cloud_config.role(roleName)
            if 'instances' not in role:
                role['instances'] = []

            instanceCount = 1
            if not role['unique']:
                instanceCount = len(instanceIdDeque)

            if device:
                createVolumes(cloud_config, roleName, instanceCount)

            while len(instanceIdDeque) > 0:
                instanceId = instanceIdDeque.popleft()
                instance = cloudutil.getNode(instanceId, conn)
                if cloudutil.isReady(instance, cloud_config.cloud()):
                    #addToEnviron(roleName, role, instance)
                    volume = getVolume(conn, cloud_config, roleName)
                    if volume:
                        attachVolume(conn, instance, volume, device)
                    volumeId = ""
                    if volume:
                        volumeId = volume.id
                    public_ip = None
                    if instance.public_ips and len(instance.public_ips) > 0:
                        public_ip = instance.public_ips[0]
                    else:
                        # Hack for futuregrid Openstack. Public ips are placed in private interface.
                        public_ip = instance.private_ips[-1]
                    role['instances'].append([instance.id, public_ip, instance.private_ips[0], volumeId])
                    preparator.submit(instance.id, 'provision', role['actions'])
                    if role['unique']:
                        break
                elif not cloudutil.isUnknown(instance):
                    instanceIdDeque.append(instanceId)
        provisionTime = time.time() - startTime
        writeEnvFile(cloud_config, open(envFile, 'w'))
        f = open(yamlLaunchFile, 'w')
        f.write(str(cloud_config))
        f.close()
    #except Exception, e:
    #    LOG.error("provision error " + str(e))
    #    raise e
    #finally:
    if True:
        preparator.join()
        endTime = time.time() - startTime
        LOG.info("TIME - Total:%f Provisioning and Preparation:%f Launch:%f" % (endTime, provisionTime, launchTime))


def preparation(cloud_config, phase, action_names, instanceIds):
    """
    prepare the instances with the given cloud configuration and action names

    :param cloud_config: The cloud config to preparation
    :type cloud_config: config.CloudConfig
    :param action_names: The names of the actions to  apply to the instances
    :param instanceIds: The instance ids to preparation
    :param instanceIds: list
    :return:
    """
    preparator = InstancePreparator(cloud_config, num_threads=40)
    startTime = time.time()
    for i in instanceIds:
        print "preparing %s" % i
        preparator.submit(i, phase, action_names.split(','))
    preparator.join()
    LOG.info("TIME - Total:%f" % (time.time() - startTime))


def main(args):
    """
    :param args:
    """
    print "FRIEDA Cloud Monkey"
    yamlFile = args[0]
    os.environ['CONFIG'] = yamlFile.split('.')[0]
    cloud_config = config.CloudConfig(yamlFile=yamlFile)
    conn = None
    instanceIds = []
    nodes = []
    phase = args[1]
    forEachNode = None    

    if len(args) >= 4 and args[3] > 0:
        nodes = args[3:]
    
    isOrca = cloud_config.cloud()['platform'] == "orca"

    # if platform is orca native API for ExoGENI, then 
    # bypass libcloud connection.

    if isOrca:
        if args[1] == "provision" or args[1] == "stop":
            orca_args = ast.literal_eval(str(args[3]))
            nodes = None
            conn = getServer(orca_args)
        else:
            instanceIds = nodes
    else:
        conn = cloudutil.getConnection(cloud_config.cloud())
        if not (args[1] == "start"):
            if nodes: #and not args[1] == "provision":
                instanceIds = nodes
            else:
                instanceIds = [n.id for n in cloudutil.getNodesForKeyname(cloud_config.cloud()['keypair'], conn)]
                
        if instanceIds:
            forEachNode = makeForEach(cloudutil.getNodesForInstanceIds(instanceIds, conn))

    if args[1] == "provision":
        if not isOrca:
            provision(cloud_config, nodes, instanceIds)
        else:
            print "chose ORCA as platform"
            provisionOrca(cloud_config, conn, orca_args, yamlFile)

    elif args[1] == "start":
        # Launch Nodes
        if not nodes:
            print " You did not select any nodes to start"
        else:
            printList(launchNodes(conn, cloud_config.cloud(), nodes))

    elif args[1] == "list":
        # List the running nodes
        if forEachNode:
            forEachNode(listNode)
        else:
            print "There are no nodes"
    elif args[1] == "stop":
        # Stop the running nodes
        if isOrca:
            deleteSlice(conn,orca_args)
        else:
            if forEachNode:
                forEachNode(destroyNode,conn)
            else:
                print "There are no nodes"
    else:
        phase = args[1]
        action_names = args[2]
        preparation(cloud_config, phase, action_names, instanceIds)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: %s <config> <action> <node/instance1> ...<node/instanceN>" % (sys.argv[0])
        exit()
    exit(main(sys.argv[1:]))


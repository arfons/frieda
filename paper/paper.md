---
title: 'FRIEDA: Flexible Robust Intelligent Elastic Data Management Framework'
tags:
  - computational science
  - cloud computing
  - data management
  - scientific data management
  - parallel and distributed systems
authors:
 - name: Devarshi Ghoshal
   orcid: 0000-0002-6819-6949
   affiliation: 1
 - name: Valerie Hendrix
   orcid: 0000-0001-9061-8952
   affiliation: 1
 - name: William Fox
   orcid: 0000-0003-1751-5118
   affiliation: 1
 - name: Sowmya Balasubhramanian
   orcid: 0000-0003-1312-9092
   affiliation: 1
 - name: Lavanya Ramakrishnan
   orcid: 0000-0003-1761-4132
   affiliation: 1
affiliations:
- name: Lawrence Berkeley National Lab
  index: 1
date: 11 Nov 2016
bibliography: paper.bib
---

# Summary
Scientific applications are increasingly using cloud resources for their data analysis workflows. However, managing data effectively and efficiently over these cloud resources is challenging due to the myriad storage choices with different performance, cost trade-offs, complex application choices and complexity associated with elasticity, failure rates in these environments. The different data access patterns for data-intensive scientific applications require a more flexible and robust data management solution than the ones currently in existence. FRIEDA is a Flexible Robust Intelligent Elastic Data Management framework that employs a range of data management strategies in cloud environments. 

FRIEDA can manage storage and data lifecycle of applications in cloud environments. There are four different stages in the data management lifecycle of FRIEDA -- i) storage planning, ii) provisioning and preparation, iii) data placement, and iv) execution.

-![FRIEDA Lifecycle](figs/lifecycle.png)

FRIEDA defines a data control plane and an execution plane. The data control plane defines the data partition and distribution strategy, whereas the execution plane manages the execution of the application using a master-worker paradigm. FRIEDA also provides different data management strategies, either to partition the data in real-time, or pre-determine the data partitions prior to application execution.

-![FRIEDA Architecture](figs/frieda_arch.png)

FRIEDA also provides a module to manage data across multiple heterogeneous cloud sites, called the FRIEDA data coordinator. The data coordinator is responsible for coordinating data movement to different cloud sites based on the properties of data and cloud instances.

-![FRIEDA Data Coordinator](figs/datacoordinator.png)

FRIEDA is released on a modified BSD license with an added paragraph at the end. FRIEDA supports cloud platforms like Amazon EC2 and OpenStack. Users need to provide cloud and application configuration information through a YAML file that is used by FRIEDA to setup and manage data in the cloud. FRIEDA provides different data management strategies for different applications. It has been tested to work efficiently with a protein sequence database ( > 6 GB), as well as for comparing light source images (> 500 GB). It is only limited by the underlying network limitations of the cloud, but allows users to configure the resources and data distribution as per the requirements. The references include papers that describe the methodologies and the experiments in detail.

# References
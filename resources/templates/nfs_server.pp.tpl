Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }


notify { "nfs-server":
		message=>"Preparing NFS Server",
	}

$exportdir='/export'
$datadir='/data'
$subnet="%(subnet)s"
$exports_content="/export/data $subnet(rw,nohide,insecure,no_subtree_check,async)"

$nfs_pkgs = $operatingsystem ? {
  centos => ['nfs-utils', 'nfs4-acl-tools', 'portmap'],
  ubuntu => ['nfs-kernel-server']
}



$nfs_service = $operatingsystem ? {
  centos => 'nfs',
  ubuntu => 'nfs-kernel-server'
}

package {$nfs_pkgs:
    ensure=>installed,
}

# Create the export directory
file { [$exportdir,"$exportdir$datadir"]:
	ensure=>directory,
	mode=>0777,
}

file { '/etc/exports':
    ensure=>present,
    content=>$exports_content,
}

# Make the file system if it hasn't been made already
exec { "mount --bind $datadir $exportdir$datadir":
       require=> [Package[$nfs_pkgs],File["$exportdir$datadir"]],
}

service { $nfs_service:
     ensure=>running,
     require=> [Package[$nfs_pkgs]],
     subscribe => [File["$exportdir$datadir",'/etc/exports']],
}

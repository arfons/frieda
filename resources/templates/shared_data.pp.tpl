Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "shared-data-notification":
		message=>"Preparing shared data",
	}
	
	
file { ['%(shared_root_dir)s','%(shared_root_dir)s/%(shared_dir)s']:
	ensure=>directory,
	owner=>"%(user)s",
	mode=>0666,
}

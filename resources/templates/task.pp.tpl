Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "task-data-notification":
		message=>"Preparing task data",
	}
	
	
file { ['%(task_root_dir)s','%(task_root_dir)s/%(task_dir)s']:
	ensure=>directory,
	owner=>"%(user)s",
	mode=>0666,
}

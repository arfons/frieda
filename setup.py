#!/usr/bin/env python
import ez_setup

ez_setup.use_setuptools()

from setuptools import setup, find_packages
import sys

python_version = sys.version_info

install_deps = []
if python_version[0] == 2:
    if python_version[1] in [6,7]:
        install_deps.append('argparse >= 1.2.1')
install_deps.append('twisted >=12.3.0')
install_deps.append('apache-libcloud')
install_deps.append('paramiko==1.9.0')
install_deps.append('pyyaml==3.10')

setup(name='FRIEDA',
      version='0.1.1',
      description='Flexible Robust Intelligent Elastic DAta management',
      author='Devarshi Ghoshal, Val Hendrix, Pradeep Mantha, Eugen Feller',
      author_email='dghoshal@lbl.gov, vchendrix@.lbl.gov, pkmantha@lbl.gov, efeller@lbl.gov',
      url='https://bosshog.lbl.gov/repos/FRIEDA',
      keywords='',
      packages=find_packages(exclude=['ez_setup', 'test', 'tools']),
      include_package_data=True,
      zip_safe=False,
      classifiers=['Development Status :: 3 - Alpha',
                   'Intended Audience :: Science/Research',
                   'Natural Language :: English',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python :: 2.7',
                   'Topic :: Scientific/Engineering',
                   'License :: OSI Approved :: BSD License'
      ],
      install_requires=install_deps,
      entry_points={'console_scripts': ['frieda = frieda:main']},
      data_files=[('resources/templates', ['resources/templates/data.pp.tpl', 'resources/templates/frieda.pp.tpl' ,
                                           'resources/templates/mount.pp.tpl', 'resources/templates/globusonline.pp.tpl',
                                           'resources/templates/mkfs.pp.tpl' ]),
                    ('resources/scripts',['resources/scripts/go_transfer.py']),
                    ('resources/configs', ['resources/configs/default.yaml']),
                    ('resources/configs', ['resources/configs/default_steps.yaml']),
                    ('frieda/execute', ['frieda/execute/config.tpl']),
                    ('', ['setup.py', 'ez_setup.py'])]
)

'''
Created on March 04, 2013

@author: pmantha
'''

import unittest
import os
import shutil
import pdb
import frieda
import frieda.plan.storage as storage

class Test(unittest.TestCase):

    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testLocal(self):
        ## Test for local storage.
        ## storage of 10G is provisioned for log on both master and worker.
       
        request = storage.StoragePlanRequest(os.path.join(os.getcwd(),"input/storage/request1.yaml"),os.path.join(os.getcwd(),"resources/resource.yaml"))
        plan = storage.StoragePlan(request)
        plan.generate_plan()
        result = plan.get_plan()
        
        expected_response = {'worker': {'task_data': {'storage_choice': 'local'}, 'log': {'storage_choice': 'local'}, 'local_storage_size': 100, 'application_source': {'storage_choice': 'local'}, 'block_nfs_storage': 0, 'output': {'storage_choice': 'block'}, 'block_storage_size': 100}, 'application': {'name': 'als'}, 'master': {'input': {'storage_choice': 'local'}, 'local_storage_choice': 100, 'log': {'storage_choice': 'local'}, 'block_storage_size': 0}, 'frieda': {'mode': 'realtime'}, 'vm': {'vm_type': 'm1.small', 'tasks_per_vm': 2, 'image_id': 'ami-0000000d', 'ssh_user': 'root', 'nfs': '/nfs', 'proposed_vm_count': 4, 'local': '/mnt', 'block': '/block'}}

        assert result == expected_response
        pass


    def testBlock(self):
        ## Test for block storage.
        ## storage for log if provisioned on both master and worker.
        ## block is provisioned for this request since the proposed_vm_count > 10 ( which is configured as BEST_NFS_BLOCK_VMS count )
        ## Since application source, shared and input data is already provisioned via block storage,
        ## the SA algo provision storage for output, task execution data
        ## storage provisioned per worker = log + margin(100MB) + nbr_tasks/proposed_vm_count * task_out_data + tasks_per_vm * ( per_task_data )
        
       
        request = storage.StoragePlanRequest(os.path.join(os.getcwd(),"input/storage/request2.yaml"),os.path.join(os.getcwd(),"resources/resource.yaml"))
        plan = storage.StoragePlan(request)
        plan.generate_plan()
        result = plan.get_plan()
        
        expected_response = {'worker': {'task_data': {'storage_choice': 'block'}, 'log': {'storage_choice': 'local'}, 'local_storage_size': 100, 'application_source': {'storage_choice': 'local'}, 'block_nfs_storage': 0, 'output': {'storage_choice': 'block'}, 'block_storage_size': 3700}, 'application': {'name': 'als'}, 'master': {'input': {'storage_choice': 'block'}, 'local_storage_choice': 100, 'log': {'storage_choice': 'local'}, 'block_storage_size': 200100}, 'frieda': {'mode': 'realtime'}, 'vm': {'vm_type': 'm1.small', 'tasks_per_vm': 2, 'image_id': 'ami-0000000d', 'ssh_user': 'root', 'nfs': '/nfs', 'proposed_vm_count': 4, 'local': '/mnt', 'block': '/block'}}
        assert result == expected_response
        pass     
    
    def testWithExistingVolume(self):
        # if the input data is already on a volume - don't provision the input data. 
         
        request = storage.StoragePlanRequest(os.path.join(os.getcwd(),"input/storage/request3.yaml"),os.path.join(os.getcwd(),"resources/resource.yaml"))
        plan = storage.StoragePlan(request)
        plan.generate_plan()
        result = plan.get_plan()
        
        expected_response = {'worker': {'task_data': {'storage_choice': 'block'}, 'log': {'storage_choice': 'local'}, 'local_storage_size': 100, 'application_source': {'storage_choice': 'local'}, 'block_nfs_storage': 0, 'output': {'storage_choice': 'block'}, 'block_storage_size': 3700}, 'application': {'name': 'als'}, 'master': {'input': {'storage_choice': 'block', 'volume_input_path': '/block/als_my_input', 'volume_id': 'vol-00000038'}, 'local_storage_choice': 100, 'log': {'storage_choice': 'local'}, 'block_storage_size': 0}, 'frieda': {'mode': 'realtime'}, 'vm': {'vm_type': 'm1.small', 'tasks_per_vm': 2, 'image_id': 'ami-0000000d', 'ssh_user': 'root', 'nfs': '/nfs', 'proposed_vm_count': 4, 'local': '/mnt', 'block': '/block'}}
        assert result == expected_response
        pass         


    def testBlockNFS(self):
        ## Test for block_nfs storage.
        ## storage for log if provisioned on both master and worker.
        ## block_nfs is provisioned for this request since the proposed_vm_count < 10 ( which is configured as BEST_NFS_BLOCK_VMS count )
        ## Since application source, shared and input data is already provisioned via block storage,
        ## the SA algo provision storage for output, task execution data
        ## since it is shared file system across all the VMS, space need to be provisioned for all tasks running on all the VMS.
        ## block_nfs space provisioned = log + margin(100MB) + nbr_tasks * task_out_data + tasks_per_vm * proposed_vm_count * ( per_task_data )
        ## Storage is also provisioned for master to store the input data.

        request = storage.StoragePlanRequest(os.path.join(os.getcwd(),"input/storage/request4.yaml"),os.path.join(os.getcwd(),"resources/resource.yaml"))
        plan = storage.StoragePlan(request)
        plan.generate_plan()
        result = plan.get_plan()
        
        expected_response = {'worker': {'task_data': {'storage_choice': 'block_nfs'}, 'log': {'storage_choice': 'local'}, 'local_storage_size': 100, 'application_source': {'storage_choice': 'local'}, 'block_nfs_storage': 10800, 'output': {'storage_choice': 'block'}, 'block_storage_size': 0}, 'application': {'name': 'als'}, 'master': {'input': {'storage_choice': 'block', 'volume_input_path': '/block/als_my_input', 'volume_id': 'vol-00000038'}, 'local_storage_choice': 100, 'log': {'storage_choice': 'local'}, 'block_storage_size': 0}, 'frieda': {'mode': 'realtime'}, 'vm': {'vm_type': 'm1.small', 'tasks_per_vm': 2, 'image_id': 'ami-0000000d', 'ssh_user': 'root', 'nfs': '/nfs', 'proposed_vm_count': 4, 'local': '/mnt', 'block': '/block'}}
        
        assert result == expected_response
        pass    
        
                
    def testPrePartition(self):
        ## Test for storage provisioning when input data resides on remote machine.
        ## storage of 10G is provisioned for log on both master and worker.
        ## block is provisioned for this request since the proposed_vm_count > 10 ( which is configured as BEST_NFS_BLOCK_VMS count )
        ## and VM local disk space is very less than the data sizes.
        ## the SA algo provision storage for output, task execution data on worker
        ## Input data is pulled by worker when needed.
        ## storage provisioned per worker = shared_data_size + log + (tasks_equally_distributed among workers in pre-partitioned case) * task_out_data + tasks_per_vm * ( per_task_data ) + margin(100MB)
        ## storage provisioned for master = log + margin (100MB)
        

        request = storage.StoragePlanRequest(os.path.join(os.getcwd(),"input/storage/request5.yaml"),os.path.join(os.getcwd(),"resources/resource.yaml"))
        plan = storage.StoragePlan(request)
        plan.generate_plan()
        result = plan.get_plan()
        
        expected_response = {'worker': {'task_data': {'storage_choice': 'block'}, 'log': {'storage_choice': 'local'}, 'local_storage_size': 100, 'application_source': {'storage_choice': 'local'}, 'block_nfs_storage': 0, 'output': {'storage_choice': 'block'}, 'block_storage_size': 3700}, 'application': {'name': 'als'}, 'master': {'block_storage_size': 0, 'local_storage_choice': 100, 'log': {'storage_choice': 'local'}}, 'frieda': {'mode': 'pre-partitioned'}, 'vm': {'vm_type': 'm1.small', 'tasks_per_vm': 2, 'image_id': 'ami-0000000d', 'ssh_user': 'root', 'nfs': '/nfs', 'proposed_vm_count': 4, 'local': '/mnt', 'block': '/block'}}
        
        assert result == expected_response
        pass   
     
     

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
